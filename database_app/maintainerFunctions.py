def addNewTool(tool_name,
				   db_params="dbname=metabolomics_database user=jonathan",
				   year_released=None,
				   website=None,
				   pub_doi=None,
				   notes=None,
				   last_updated=None,
				   annotation=None,
				   approaches=None,
				   inputs=None,
				   outputs=None,
				   instrument=None,
				   licenses=None,
				   os=None,
				   proc=None,
				   statistic=None,
				   ui=None,
				   programming=None,
				   applications=None,
				   containers=None,
				   formats=None,
				   functionalities=None,
				   molecules=None
				   ):
	"""
	Function that adds new tools into the database
	inputs: db_params a string of the database params/credentials
	        tool_name a string name of the tool to be added
	        year_released a string of the numerical releasebyear of the tool
	        website a string of the website of the tool
	        pub_doi a string doi of the publication for the tool
	        notes a string mainter tool notes
	        last_updated a string data of the last update to the tool
	        annotation a string of the annotation method of the tool
	        approaches a string of the approaches of the tool
	        inputs a strng of the input variables of the tool
	        outputs a string of the output variables of the tool
	        instument a string of the instrument type the tool works with
	        licenses a string of the licenses of the tool
	        os a string of the operating systems the tool is available for
	        proc a string of the processing methods of the tool
	        statistic a string of the statistical methods of the tool
	        ui a string of the user interface of the tool
	        programming a strng of the available programming languages for the tool
	        applications a string for the applications of the tool
	        containers a string for the available containers for the tool
	        formats a string for the file formats of the tool
	        functionalities a string for the functionalities of the tool
	        molecules a string for the moleculs the tool works with
	Output: Adds tool information to appropriate tables in the database
	Note: Multiple input arguments are separated by a ','"""
	import psycopg2
	import re
	from datetime import date
	if pub_doi is not None and pub_doi != "":
		pub_doi = re.sub(" +,", ",", pub_doi)
		pub_doi = re.sub(" +", " ", pub_doi)
		pub_doi = pub_doi.split(sep=", ")
	if annotation is not None and annotation != "":
		annotation = re.sub(" +,", ",", annotation)
		annotation = re.sub(" +", " ", annotation)
		annotation = annotation.split(sep=", ")
	if approaches is not None and approaches != "":
		approaches = re.sub(" +,", ",", approaches)
		approaches = re.sub(" +", " ", approaches)
		approaches = approaches.split(sep=", ")
	if inputs is not None and inputs != "":
		inputs = re.sub(" +,", ",", inputs)
		inputs = re.sub(" +", " ", inputs)
		inputs = inputs.split(sep=", ")
	if outputs is not None and outputs != "":
		outputs = re.sub(" +,", ",", outputs)
		outputs = re.sub(" +", " ", outputs)
		outputs = outputs.split(sep=", ")
	if instrument is not None and instrument != "":
		instrument = re.sub(" +,", ",", instrument)
		instrument = re.sub(" +", " ", instrument)
		instrument = instrument.split(sep=", ")
	if licenses is not None and licenses != "":
		licenses = re.sub(" +,", ",", licenses)
		licenses = re.sub(" +", " ", licenses)
		licenses = licenses.split(sep=", ")
	if os is not None and os != "":
		os = re.sub(" +,", ",", os)
		os = re.sub(" +", " ", os)
		os = os.title()
		os = os.split(sep=", ")
	if  proc is not None and proc != "":
		proc = re.sub(" +,", ",", proc)
		proc = re.sub(" +", " ", proc)
		proc = proc.split(sep=", ")
	if statistic is not None and statistic != "":
		statistic = re.sub(" +,", ",", statistic)
		statistic = re.sub(" +", " ", statistic)
		statistic = statistic.split(sep=", ")
	if ui is not None and ui != "":
		ui = re.sub(" +,", ",", ui)
		ui = re.sub(" +", " ", ui)
		ui = ui.split(sep=", ")
		ui = [x.upper() if x.lower() == "gui" or x.lower() == "cli" else x.lower() for x in ui]
	if programming is not None and programming != "":
		programming = re.sub(" +,", ",", programming)
		programming = re.sub(" +", " ", programming)
		programming = programming.split(sep=", ")
	if applications is not None and applications != "":
		applications = re.sub(" +,", ",", applications)
		applications = re.sub(" +", " ", applications)
		applications = applications.split(sep  = ", ")
	if containers is not None and containers != "":
		containers = re.sub(" +,", ",", containers)
		containers = re.sub(" +", " ", containers)
		containers = containers.split(sep = ", ")
	if formats is not None and formats != "":
		formats = re.sub(" +,", ",", formats)
		formats = re.sub(" +", " ", formats)
		formats = formats.split(sep = ", ")
	if functionalities is not  None and functionalities != "":
		functionalities = re.sub(" +,", ",", functionalities)
		functionalities = re.sub(" +", " ", functionalities)
		functionalities  =  functionalities.split(sep = ", ")
	if molecules is not None and molecules != "":
		molecules = re.sub(" +,", ",", molecules)
		molecules = re.sub(" +", " ", molecules)
		molecules = molecules.split(sep = ", ")
	 # Connect to a database that exists
	conn = psycopg2.connect(db_params)
	# Open a cursor to perform database operations
	cur = conn.cursor()

	# Restrictions
	# Tool name 
	cur.execute("SELECT tool_name FROM main WHERE LOWER(tool_name) = '{}';".format(
		tool_name.lower()))
	x = cur.fetchall()
	if x:
		raise ValueError("Tool Name already exists in the database!")
	# Year released
	if (year_released !="" and year_released is not None) and (not all(letter.isdigit() for letter in year_released) or len(year_released)!=4):
		raise ValueError("Year released must only contain numeric characters, and must contain 4 digits!")
	#DOI
	# if pub_doi!="" and pub_doi is not None:
	# 	pattern = re.compile("\d\d.\d\d\d\d")
	# 	doi_num = re.findall(".org/(.*?)[/%]", pub_doi)
	# 	if not doi_num or not pattern.match(doi_num[0]):
	# 		raise ValueError("Incorrect DOI format!")
	# if (pub_doi!="" and pub_doi is not None) and (pub_doi[:4] not in ['http', 'https']):
	# 	pub_doi = "https://" + pub_doi
	# Last Updated
	if (last_updated!="" and last_updated is not None):
		pattern = re.compile("\d\d\d\d-\d\d-\d\d")
		if not pattern.match(last_updated):
			raise ValueError("Incorrect date format, must be year-month-day, hyphens required")
	# OS
	os_list = [x.lower() for x in os]
	os_types = ['windows', 'mac', 'linux']
	os_diff = list(set(os_list)-set(os_types))
	if (os!="" and os is not None) and (os_diff):
		raise ValueError(os_diff, "is/are not valid operating system(s)")

	#UI
	ui_list = [x.lower() for x in ui]
	ui_types = ['cli', 'gui', 'web']
	ui_diff = list(set(ui_list)-set(ui_types))
	if (ui!="" and ui is not None) and (ui_diff):
		raise ValueError(ui_diff, "is/are not valid user interface(s)")

	print(pub_doi)


	cur.execute("INSERT INTO main(tool_name, year_released, website, date_modified, notes, last_updated) VALUES('{}', {}, '{}', '{}', '{}', {});".format(
		tool_name, year_released if year_released != "" else 'null', website, date.today().strftime('%Y-%m-%d'), notes, "'" + last_updated + "'" if last_updated != "" else 'null'))
	if pub_doi is not None:
		for i in range(len(pub_doi)):
			cur.execute("INSERT INTO publications(tool_name, pub_doi) VALUES('{}', '{}')".format(tool_name, pub_doi[i]))
	if annotation is not None:
		for i in range(len(annotation)):
			cur.execute("INSERT INTO func_annotation(tool_name, annotation) VALUES('{}', '{}')".format(tool_name, annotation[i]))
	if approaches is not None:
		for i in range(len(approaches)):
			cur.execute("INSERT INTO approach(tool_name, approach) VALUES('{}', '{}')".format(tool_name, approaches[i]))
	if inputs is not None:
		for i in range(len(inputs)):
			cur.execute("INSERT INTO input_variables(tool_name, input) VALUES('{}', '{}')".format(tool_name, inputs[i]))
	if outputs is not None:
		for i in range(len(outputs)):
			cur.execute("INSERT INTO output_variables(tool_name, output) VALUES('{}', '{}')".format(tool_name, outputs[i]))
	if instrument is not None:
		for i in range(len(instrument)):
			cur.execute("INSERT INTO instrument_data(tool_name, instrument) VALUES('{}', '{}')".format(tool_name, instrument[i]))
	if licenses is not None:
		for i in range(len(licenses)):
			cur.execute("INSERT INTO software_license(tool_name, license) VALUES('{}', '{}')".format(tool_name, licenses[i]))
	if os is not None:
		for i in range(len(os)):
			cur.execute("INSERT INTO os_options(tool_name, os) VALUES('{}', '{}')".format(tool_name, os[i]))
	if proc is not None:
		for i in range(len(proc)):
			cur.execute("INSERT INTO func_processing(tool_name, processing) VALUES('{}', '{}')".format(tool_name, proc[i]))
	if statistic is not None:
		for i in range(len(statistic)):
			cur.execute("INSERT INTO func_statistics(tool_name, stats_method) VALUES('{}', '{}')".format(tool_name, statistic[i]))
	if ui is not None:
		for i in range(len(ui)):
			cur.execute("INSERT INTO ui_options(tool_name, ui) VALUES('{}', '{}')".format(tool_name, ui[i]))
	if programming is not None:
		for i in range(len(programming)):
			cur.execute("INSERT INTO programming_language(tool_name, language) VALUES('{}', '{}')".format(tool_name, programming[i]))
	if applications is not None:
		for i in range(len(applications)):
			cur.execute("INSERT INTO application(tool_name, domain) VALUES('{}', '{}')".format(tool_name, applications[i]))
	if containers is not None:
		for i in range(len(containers)):
			cur.execute("INSERT INTO containers(tool_name, container_url) VALUES('{}', '{}')".format(tool_name, containers[i]))
	if formats is not None:
		for i in range(len(formats)):
			cur.execute("INSERT INTO file_formats(tool_name, format) VALUES('{}', '{}')".format(tool_name, formats[i]))
	if functionalities is not None:
		for i in range(len(functionalities)):
			cur.execute("INSERT INTO functionality(tool_name, function) VALUES('{}', '{}')".format(tool_name, functionalities[i]))
	if molecules is not None:
		for i in range(len(molecules)):
			cur.execute("INSERT INTO molecule_type(tool_name, moltype) VALUES('{}', '{}')".format(tool_name, molecules[i]))
	conn.commit()
	conn.close()

def addToolFeatures(tool_name,
					db_params="dbname=metabolomics_database user=jonathan",
					year_released=None,
					website=None,
					pub_doi=None,
					notes=None,
					last_updated=None,
					annotation=None,
					approaches=None,
					inputs=None,
					outputs=None,
					instrument=None,
					licenses=None,
					os=None,
					proc=None,
					statistic=None,
					ui=None,
					programming=None,
					applications=None,
					containers=None,
					formats=None,
					functionalities=None,
					molecules=None):
	"""
	Function that updates tools in the database
	inputs: db_params a string of the database params/credentials
	        tool_name a string name of the tool to be updated
	        year_released a string of the numerical releasebyear of the tool
	        website a string of the website of the tool
	        pub_doi a string doi of the publication for the tool
	        notes a string mainter tool notes
	        last_updated a string data of the last update to the tool
	        annotation a string of the annotation method of the tool
	        approaches a string of the approaches of the tool
	        inputs a strng of the input variables of the tool
	        outputs a string of the output variables of the tool
	        instument a string of the instrument type the tool works with
	        licenses a string of the licenses of the tool
	        os a string of the operating systems the tool is available for
	        proc a string of the processing methods of the tool
	        statistic a string of the statistical methods of the tool
	        ui a string of the user interface of the tool
	        programming a strng of the available programming languages for the tool
	        applications a string for the applications of the tool
	        containers a string for the available containers for the tool
	        formats a string for the file formats of the tool
	        functionalities a string for the functionalities of the tool
	        molecules a string for the moleculs the tool works with
	Output: updates tool information in the appropriate tables in the database
	Note: Multiple input arguments are separated by a ","
	"""
	import psycopg2
	import re
	from datetime import date
	if pub_doi is not None and pub_doi != "":
		pub_doi = re.sub(" +,", ",", pub_doi)
		pub_doi = re.sub(" +", " ", pub_doi)
		pub_doi = pub_doi.split(sep=", ")
	if annotation is not None and annotation != "":
		annotation = re.sub(" +,", ",", annotation)
		annotation = re.sub(" +", " ", annotation)
		annotation = annotation.split(sep=", ")
	if approaches is not None and approaches != "":
		approaches = re.sub(" +,", ",", approaches)
		approaches = re.sub(" +", " ", approaches)
		approaches = approaches.split(sep=", ")
	if inputs is not None and inputs != "":
		inputs = re.sub(" +,", ",", inputs)
		inputs = re.sub(" +", " ", inputs)
		inputs = inputs.split(sep=", ")
	if outputs is not None and outputs != "":
		outputs = re.sub(" +,", ",", outputs)
		outputs = re.sub(" +", " ", outputs)
		outputs = outputs.split(sep=", ")
	if instrument is not None and instrument != "":
		instrument = re.sub(" +,", ",", instrument)
		instrument = re.sub(" +", " ", instrument)
		instrument = instrument.split(sep=", ")
	if licenses is not None and licenses != "":
		licenses = re.sub(" +,", ",", licenses)
		licenses = re.sub(" +", " ", licenses)
		licenses = licenses.split(sep=", ")
	if os is not None and os != "":
		os = re.sub(" +,", ",", os)
		os = re.sub(" +", " ", os)
		os = os.title()
		os = os.split(sep=", ")
	if proc is not None and proc != "":
		proc = re.sub(" +,", ",", proc)
		proc = re.sub(" +", " ", proc)
		proc = proc.split(sep=", ")
	if statistic is not None and statistic != "":
		statistic = re.sub(" +,", ",", statistic)
		statistic = re.sub(" +", " ", statistic)
		statistic = statistic.split(sep=", ")
	if ui is not None and ui != "":
		ui = re.sub(" +,", ",", ui)
		ui = re.sub(" +", " ", ui)
		ui = ui.split(sep=", ")
		ui = [x.upper() if x.lower() == "gui" or x.lower() == "cli" else x.lower() for x in ui]
	if programming is not None and programming != "":
		programming = re.sub(" +,", ",", programming)
		programming = re.sub(" +", " ", programming)
		programming = programming.split(sep=", ")
	if applications is not None and applications != "":
		applications = re.sub(" +,", ",", applications)
		applications = re.sub(" +", " ", applications)
		applications = applications.split(sep  = ", ")
	if containers is not None and containers != "":
		containers = re.sub(" +,", ",", containers)
		containers = re.sub(" +", " ", containers)
		containers = containers.split(sep = ", ")
	if formats is not None and formats != "":
		formats = re.sub(" +,", ",", formats)
		formats = re.sub(" +", " ", formats)
		formats = formats.split(sep = ", ")
	if functionalities is not  None and functionalities != "":
		functionalities = re.sub(" +,", ",", functionalities)
		functionalities = re.sub(" +", " ", functionalities)
		functionalities  =  functionalities.split(sep = ", ")
	if molecules is not None and molecules != "":
		molecules = re.sub(" +,", ",", molecules)
		molecules = re.sub(" +", " ", molecules)
		molecules = molecules.split(sep = ", ")
	 # Connect to a database that exists
	conn = psycopg2.connect(db_params)
	# Open a cursor to perform database operations
	cur = conn.cursor()
	# Restrictions
	# Tool name 
	cur.execute("SELECT tool_name FROM main WHERE LOWER(tool_name) = '{}';".format(
		tool_name.lower()))
	x = cur.fetchall()
	# Year released
	if (year_released !="" and year_released is not None) and (not all(letter.isdigit() for letter in year_released) or len(year_released)!=4):
		raise ValueError("Year released must only contain numeric characters, and must contain 4 digits!")
	#DOI
	# if pub_doi!="" and pub_doi is not None:
	# 	pattern = re.compile("\d\d.\d\d\d\d")
	# 	doi_num = re.findall(".org/(.*?)[/%]", pub_doi)
	# 	if not doi_num or not pattern.match(doi_num[0]):
	# 		raise ValueError("Incorrect DOI format!")
	# if (pub_doi!="" and pub_doi is not None) and (pub_doi[:4] not in ['http', 'https']):
	# 	pub_doi = "https://" + pub_doi
	# Last Updated
	if (last_updated!="" and last_updated is not None):
		pattern = re.compile("\d\d\d\d-\d\d-\d\d")
		if not pattern.match(last_updated):
			raise ValueError("Incorrect date format, must be year-month-day, hyphens required")
	# OS
	os_list = [x.lower() for x in os]
	os_types = ['windows', 'mac', 'linux']
	os_diff = list(set(os_list)-set(os_types))
	if (os!="" and os is not None) and (os_diff):
		raise ValueError(os_diff, "is/are not valid operating system(s)")

	#UI
	ui_list = [x.lower() for x in ui]
	ui_types = ['cli', 'gui', 'web']
	ui_diff = list(set(ui_list)-set(ui_types))
	if (ui!="" and ui is not None) and (ui_diff):
		raise ValueError(ui_diff, "is/are not valid user interface(s)")
	if x:
		cur.execute("DELETE from main WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from func_annotation WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from approach WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from input_variables WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from output_variables WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from instrument_data WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from software_license WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from os_options WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from processing WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from func_statistics WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from ui_options WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from programming_language WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from application WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from containers WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from file_formats WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from functionality WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from molecule_type WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from publications WHERE tool_name = '{}';".format(tool_name))
		cur.execute("INSERT INTO main(tool_name, year_released, website, date_modified, notes, last_updated) VALUES('{}', {}, '{}', '{}', '{}', {});".format(
		tool_name, year_released if year_released != "" else 'null', website, date.today().strftime('%Y-%m-%d'), notes, "'" + last_updated + "'" if last_updated != "" else 'null'))
		if pub_doi is not None:
			for i in range(len(pub_doi)):
				cur.execute("INSERT INTO publications(tool_name, pub_doi) VALUES('{}', '{}')".format(tool_name, pub_doi[i]))
		if annotation is not None:
			for i in range(len(annotation)):
				cur.execute("INSERT INTO func_annotation(tool_name, annotation) VALUES('{}', '{}')".format(tool_name, annotation[i]))
		if approaches is not None:
			for i in range(len(approaches)):
				cur.execute("INSERT INTO approach(tool_name, approach) VALUES('{}', '{}')".format(tool_name, approaches[i]))
		if inputs is not None:
			for i in range(len(inputs)):
				cur.execute("INSERT INTO input_variables(tool_name, input) VALUES('{}', '{}')".format(tool_name, inputs[i]))
		if outputs is not None:
			for i in range(len(outputs)):
				cur.execute("INSERT INTO output_variables(tool_name, output) VALUES('{}', '{}')".format(tool_name, outputs[i]))
		if instrument is not None:
			for i in range(len(instrument)):
				cur.execute("INSERT INTO instrument_data(tool_name, instrument) VALUES('{}', '{}')".format(tool_name, instrument[i]))
		if licenses is not None:
			for i in range(len(licenses)):
				cur.execute("INSERT INTO software_license(tool_name, license) VALUES('{}', '{}')".format(tool_name, licenses[i]))
		if os is not None:
			for i in range(len(os)):
				cur.execute("INSERT INTO os_options(tool_name, os) VALUES('{}', '{}')".format(tool_name, os[i]))
		if proc is not None:
			for i in range(len(proc)):
				cur.execute("INSERT INTO func_processing(tool_name, processing) VALUES('{}', '{}')".format(tool_name, proc[i]))
		if statistic is not None and statistic != "":
			for i in range(len(statistic)):
				cur.execute("INSERT INTO func_statistics(tool_name, stats_method) VALUES('{}', '{}')".format(tool_name, statistic[i]))
		if ui is not None:
			for i in range(len(ui)):
				cur.execute("INSERT INTO ui_options(tool_name, ui) VALUES('{}', '{}')".format(tool_name, ui[i]))
		if programming is not None:
			for i in range(len(programming)):
				cur.execute("INSERT INTO programming_language(tool_name, language) VALUES('{}', '{}')".format(tool_name, programming[i]))
		if applications is not None:
			for i in range(len(applications)):
				cur.execute("INSERT INTO application(tool_name, domain) VALUES('{}', '{}')".format(tool_name, applications[i]))
		if containers is not None:
			for i in range(len(containers)):
				cur.execute("INSERT INTO containers(tool_name, container_url) VALUES('{}', '{}')".format(tool_name, containers[i]))
		if formats is not None:
			for i in range(len(formats)):
				cur.execute("INSERT INTO file_formats(tool_name, format) VALUES('{}', '{}')".format(tool_name, formats[i]))
		if functionalities is not None:
			for i in range(len(functionalities)):
				cur.execute("INSERT INTO functionality(tool_name, function) VALUES('{}', '{}')".format(tool_name, functionalities[i]))
		if molecules is not None:
			for i in range(len(molecules)):
				cur.execute("INSERT INTO molecule_type(tool_name, moltype) VALUES('{}', '{}')".format(tool_name, molecules[i]))
		# Update rowid for tool across all tables in database
		cur.execute("SELECT setval(pg_get_serial_sequence('func_annotation', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM func_annotation;")
		cur.execute("UPDATE func_annotation SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE func_annotation_rowid_seq RESTART")
		cur.execute("SELECT setval(pg_get_serial_sequence('publications', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM publications;")
		cur.execute("UPDATE publications SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE publications_rowid_seq RESTART")
		cur.execute("UPDATE func_annotation SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('approach', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM approach;")
		cur.execute("UPDATE approach SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE approach_rowid_seq RESTART")
		cur.execute("UPDATE approach SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('input_variables', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM input_variables;")
		cur.execute("UPDATE input_variables SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE input_variables_rowid_seq RESTART")
		cur.execute("UPDATE input_variables SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('output_variables', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM output_variables;")
		cur.execute("UPDATE output_variables SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE formats_output_rowid_seq RESTART")
		cur.execute("UPDATE output_variables SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('instrument_data', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM instrument_data;")
		cur.execute("UPDATE instrument_data SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE instrument_data_rowid_seq RESTART")
		cur.execute("UPDATE instrument_data SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('software_license', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM software_license;")
		cur.execute("UPDATE software_license SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE software_license_rowid_seq RESTART")
		cur.execute("UPDATE software_license SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('os_options', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM os_options;")
		cur.execute("UPDATE os_options SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE os_options_rowid_seq RESTART")
		cur.execute("UPDATE os_options SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('func_processing', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM func_processing;")
		cur.execute("UPDATE func_processing SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE func_processing_rowid_seq RESTART")
		cur.execute("UPDATE func_processing SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('func_statistics', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM func_statistics;")
		cur.execute("UPDATE func_statistics SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE func_statistics_rowid_seq RESTART")
		cur.execute("UPDATE func_statistics SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('ui_options', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM ui_options;")
		cur.execute("UPDATE ui_options SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE ui_options_rowid_seq RESTART")
		cur.execute("UPDATE ui_options SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('programming_language', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM programming_language;")
		cur.execute("UPDATE programming_language SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE programming_language_rowid_seq RESTART")
		cur.execute("UPDATE programming_language SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('application', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM application;")
		cur.execute("UPDATE application SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE approach_domain_rowid_seq RESTART")
		cur.execute("UPDATE application SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('containers', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM containers;")
		cur.execute("UPDATE containers SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE containers_rowid_seq RESTART")
		cur.execute("UPDATE containers SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('file_formats', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM file_formats;")
		cur.execute("UPDATE file_formats SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE file_formats_rowid_seq RESTART")
		cur.execute("UPDATE file_formats SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('functionality', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM functionality;")
		cur.execute("UPDATE functionality SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE functionality_rowid_seq RESTART")
		cur.execute("UPDATE functionality SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('molecule_type', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM molecule_type;")
		cur.execute("UPDATE molecule_type SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE approach_moltype_rowid_seq RESTART")
		cur.execute("UPDATE molecule_type SET rowid = DEFAULT")
	else:
		raise ValueError("Tool Name does not exist in database!")
	conn.commit()
	conn.close()

def deleteTool(tool_name,db_params="dbname=metabolomics_database user=jonathan"):
	"""
	Function that deletes a tool in the database
	inputs: db_params a string of the database params/credentials
            tool_name a string name of the tool to be deleted
    output: Delete tool from all tables in the database
	"""
	import psycopg2
	conn = psycopg2.connect(db_params)
	cur = conn.cursor()
	cur.execute("SELECT tool_name FROM main WHERE tool_name = '{}';".format(
		tool_name))
	x = cur.fetchall()
	if x:
		cur.execute("DELETE from main WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from func_annotation WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from approach WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from input_variables WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from output_variables WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from instrument_data WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from software_license WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from os_options WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from func_processing WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from func_statistics WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from ui_options WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from programming_language WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from application WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from containers WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from file_formats WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from functionality WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from molecule_type WHERE tool_name = '{}';".format(tool_name))
		cur.execute("DELETE from publications WHERE tool_name = '{}';".format(tool_name))
		cur.execute("SELECT setval(pg_get_serial_sequence('func_annotation', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM func_annotation;")
		cur.execute("UPDATE func_annotation SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE func_annotation_rowid_seq RESTART")
		cur.execute("UPDATE func_annotation SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('approach', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM approach;")
		cur.execute("UPDATE approach SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE approach_rowid_seq RESTART")
		cur.execute("UPDATE approach SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('input_variables', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM input_variables;")
		cur.execute("UPDATE input_variables SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE input_variables_rowid_seq RESTART")
		cur.execute("UPDATE input_variables SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('output_variables', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM output_variables;")
		cur.execute("UPDATE output_variables SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE formats_output_rowid_seq RESTART")
		cur.execute("UPDATE output_variables SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('instrument_data', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM instrument_data;")
		cur.execute("UPDATE instrument_data SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE instrument_data_rowid_seq RESTART")
		cur.execute("UPDATE instrument_data SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('software_license', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM software_license;")
		cur.execute("UPDATE software_license SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE software_license_rowid_seq RESTART")
		cur.execute("UPDATE software_license SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('os_options', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM os_options;")
		cur.execute("UPDATE os_options SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE os_options_rowid_seq RESTART")
		cur.execute("UPDATE os_options SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('func_processing', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM func_processing;")
		cur.execute("UPDATE func_processing SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE func_processing_rowid_seq RESTART")
		cur.execute("UPDATE func_processing SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('func_statistics', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM func_statistics;")
		cur.execute("UPDATE func_statistics SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE func_statistics_rowid_seq RESTART")
		cur.execute("UPDATE func_statistics SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('ui_options', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM ui_options;")
		cur.execute("UPDATE ui_options SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE ui_options_rowid_seq RESTART")
		cur.execute("UPDATE ui_options SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('programming_language', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM programming_language;")
		cur.execute("UPDATE programming_language SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE programming_language_rowid_seq RESTART")
		cur.execute("UPDATE programming_language SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('application', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM application;")
		cur.execute("UPDATE application SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE approach_domain_rowid_seq RESTART")
		cur.execute("UPDATE application SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('containers', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM containers;")
		cur.execute("UPDATE containers SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE containers_rowid_seq RESTART")
		cur.execute("UPDATE containers SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('file_formats', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM file_formats;")
		cur.execute("UPDATE file_formats SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE file_formats_rowid_seq RESTART")
		cur.execute("UPDATE file_formats SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('functionality', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM functionality;")
		cur.execute("UPDATE functionality SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE functionality_rowid_seq RESTART")
		cur.execute("UPDATE functionality SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('molecule_type', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM molecule_type;")
		cur.execute("UPDATE molecule_type SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE approach_moltype_rowid_seq RESTART")
		cur.execute("UPDATE molecule_type SET rowid = DEFAULT")
		cur.execute("SELECT setval(pg_get_serial_sequence('publications', 'rowid'), coalesce(max(rowid),0) + 1, false) FROM publications;")
		cur.execute("UPDATE publications SET rowid = DEFAULT")
		cur.execute("ALTER SEQUENCE publications_rowid_seq RESTART")
		conn.commit()
		conn.close()