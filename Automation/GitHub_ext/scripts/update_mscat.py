# -------------------------------
# update_mscat.py
# Script that collects the latest data.
# latest data is obtained by source packages like github and git
# creates two .csv files (updated_mscat.csv, new_data.csv)
# -------------------------------
import os
import warnings
from environs import Env
from collections import Counter

# dependencies imports
import pandas as pd
import numpy as np
from sqlalchemy import create_engine

# github_ext imports
from github_ext.apis.github import GitHubClient
from github_ext.apis.git_client import GitClient
from github_ext.apis.urls import BioconEndPoints
from github_ext.apis.url_patterns import UrlPatterns
from github_ext.mscat_utils.utils import clean_links, link_search, style_links
from github_ext.mscat_utils.link_utils import search_source_links
from github_ext.mscat_utils.github_ext_paths import GithubExtPaths
from github_ext.parsers import biocon_parser

# ignoring pandas warnings
warnings.filterwarnings("ignore")

pattern = UrlPatterns()


def check_api() -> None:
    """Checks if api variables exists"""
    envs = Env()

    envs("GITHUB_KEY")
    return None


def collect_data(url):
    """Collects data from both github and git and returns the
    to language, number of commits, number of contributors and
    the license type.

    Parameters
    ----------
    url : str
        link containing to the source code

    Returns
    -------
    list
        list containing top languages, latest update date, number of
        commits, number of contributors, license type.
    """
    url = url.strip()
    pattern = UrlPatterns()

    print(f"Collecting source code data from: {url}")

    if url is np.nan:
        return [np.nan, np.nan, np.nan, np.nan, np.nan]

    elif url.startswith(pattern.github_link):
        conts = tuple(url.split("/"))
        owner, repo = conts[3], conts[4]
        try:
            github = GitHubClient(owner, repo, timeout=20)
            top_lang = github.get_all_languages()
            latest_date = github.get_latest_commit().commit_date
            n_commits = github.get_total_commits()
            n_contributors = github.get_total_contributors()
            license_type = github.repo.license_type
            results = [top_lang, latest_date, n_commits, n_contributors, license_type]
            return results
        except:
            print("ERROR: {}".format(url))
            results = [np.nan, np.nan, np.nan, np.nan, np.nan]
            return results

    # working bioconductor
    elif pattern.bioconductor_package in url:
        tar_fname = url.rsplit("/", 1)[-1]
        tool_name = tar_fname.split("_")[0]
        biocon_endpoints = BioconEndPoints(tool_name)

        try:
            # using perceval to download git data and extract data
            git_url = os.path.join(pattern.bioconductor_git, tool_name)
            git = GitClient(git_url, tmp_name=tool_name)
            top_lang = git.all_languages()
            latest_date = git.latest_commit_date()
            n_commits = git.total_commits()
            n_contributors = git.n_contributors()

            license_type = biocon_parser.get_license(biocon_endpoints.bioconductor_page)
            git.remove_cache()

            results = [top_lang, latest_date, n_commits, n_contributors, license_type]
            return results
        except:
            results = [np.nan, np.nan, np.nan, np.nan, np.nan]
            return results
    else:
        results = [np.nan, np.nan, np.nan, np.nan, np.nan]
        return results


if __name__ == "__main__":

    check_api()

    # loading in paths
    gh_paths = GithubExtPaths()

    # importing latest data
    # mscat_data = gh_paths.db_entries
    # mscat_df = pd.read_csv(mscat_data)

    # Connect to a database that exists
    ## Database connection settings (should be moved to envir variables)
    engine = create_engine("postgresql+psycopg2://postgres:postgres@pgsql/postgres")

    # importing latest data
    mscat_df = pd.read_sql("SELECT * FROM main", engine)

    # Formatting and data cleaning
    # -- remove HTML/CSS styling ofl inks
    mscat_df["Website"] = mscat_df["website"].apply(clean_links)

    # ALL GITHUB WEBSITES
    # -- loading all url patterns for searching
    pattern = UrlPatterns()

    # -- removing rows that do not have websites
    edited_mscat_df = mscat_df.dropna(subset=["Website"])

    # -- all entries that have github link
    github_df = edited_mscat_df.loc[
        edited_mscat_df["Website"].str.startswith(pattern.github_link)
    ]

    # ALL BIOCONDUCTOR WEBSITES
    # bioconductor_df = edited_mscat_df["Website"].apply(lambda df: link_search(df["Website"], pattern=pattern.bioconductor_package))
    bioconductor_bool_mask = edited_mscat_df["Website"].apply(
        lambda df: link_search(df, pattern=pattern.bioconductor_package)
    )
    bioconductor_df = edited_mscat_df[bioconductor_bool_mask]

    # ALL OTHER WEBSITES
    # -- using bitwise operator  ~ = Not
    # -- removing all github and bioconductor links
    # home_page_df = edited_mscat_df.loc[~edited_mscat_df["Website"].str.startswith(pattern.github_link)]
    home_page_df = edited_mscat_df.loc[~bioconductor_bool_mask]
    home_page_df = home_page_df.loc[
        ~edited_mscat_df["Website"].str.startswith(pattern.github_link)
    ]

    # Github
    # Since the website link is a git hub link, the "source" column will have the github
    # and location will be "MSCAT"
    github_df["source"] = github_df["Website"].values
    github_df["location"] = "source"

    # -- checking coverage
    has_github = (github_df["source"].isnull() == False).sum()
    coverage = round(has_github / len(github_df), 2) * 100
    print("Github Coverage: {}%".format(coverage))

    # Bioconductor
    # This there is a possibility that bioconductor may not have a github url,
    # then we focus on using the git url to extract meta data via perceval package
    # [IMPORTANT] HERE WE ASSUME THAT EVERY PACKAGE IS WRITTEN IN R

    # -- applying github link searching algorithm to all websites
    bioconductor_df["source"] = bioconductor_df[["tool_name", "Website"]].apply(
        lambda df: search_source_links(df["tool_name"], df["Website"]), axis=1
    )
    bioconductor_df["location"] = "source"

    # -- -- checking the coverage
    has_git = (bioconductor_df["source"].isnull() == False).sum()
    coverage = round(has_git / len(bioconductor_df), 2) * 100
    print("Bioconductor coverage: {}%".format(coverage))

    # Homepage links
    home_page_df["source"] = home_page_df[["tool_name", "Website"]].apply(
        lambda df: search_source_links(df["tool_name"], df["Website"]), axis=1
    )
    home_page_df["location"] = "landing_page"
    # -- checking coverage
    has_source = (home_page_df["source"].isnull() == False).sum()
    coverage = round(has_source / len(home_page_df), 2) * 100
    print("homepage coverage: {}".format(coverage))

    concat_df = pd.concat([github_df, bioconductor_df, home_page_df])

    cnt_data = Counter(concat_df.index.tolist())
    dup_check = cnt_data.most_common()[0][1]
    if dup_check > 1:
        index, counts = cnt_data.most_common()
        raise IndexError(
            "Duplicate indices found! Index: {}, Count: {},".format(index, counts)
        )
    # Joining data set with original csv file via index numbering
    new_df = mscat_df.merge(concat_df, how="outer", on=["tool_name"])
    new_df = new_df[["location", "source"]]
    new_mscat_df = pd.concat([mscat_df, new_df], axis=1)

    # ------------------------------
    # step 2: Collecting the data from github and git
    # ------------------------------

    # total number of entries that have a source links
    df = new_mscat_df.loc[new_mscat_df["source"].notnull()]
    n_entries = df.shape[0]
    print("Total amount of entries that have source code links {}".format(n_entries))

    # collecting the source code data from source link
    df[["all_lang", "latest_date", "n_commits", "n_contributors", "license_type"]] = (
        df["source"]
        .to_frame()
        .apply(lambda df: collect_data(df["source"]), axis=1, result_type="expand")
    )

    # new data frame with collected data for source link
    new_fields_df = df[
        [
            "tool_name",
            "location",
            "source",
            "all_lang",
            "latest_date",
            "n_commits",
            "n_contributors",
            "license_type",
        ]
    ]

    # ------------------------------
    # Step 3: Merging the data set with mscat df
    # ------------------------------
    # Merging original MSCAT data frame with the collected data
    updated_mscat_df = mscat_df.merge(new_fields_df, how="left", on="tool_name")

    # Re-formatting links (HTML styling)
    updated_mscat_df["Website"] = (
        updated_mscat_df["Website"].astype(str).apply(style_links)
    )

    # only extracting the tool_name, latest update date, all languages
    updated_df = updated_mscat_df[["tool_name", "latest_date", "all_lang"]]

    # removing rows that contains NA in it in the "latest_date" column
    cleaned_updated_df = updated_df.dropna()
    cleaned_updated_df = cleaned_updated_df.loc[
        cleaned_updated_df["all_lang"].notnull()
    ]

    # save file
    # cleaned_updated_df.to_csv("cleaned_updated_data.csv", index=False)

    # Update database entries
    # need these to be separate because all_lang is an array
    date_df = cleaned_updated_df[["tool_name", "latest_date"]].copy()
    lang_df = cleaned_updated_df[["tool_name", "all_lang"]].copy()
    lang_df["languages"] = lang_df["all_lang"].str.split(" ")
    # Need to explode all_lang
    lang_df = lang_df[["tool_name", "languages"]].explode("languages")
    date_df.to_sql("temp_date", con=engine, if_exists="replace")
    lang_df.to_sql("temp_lang", con=engine, if_exists="replace")

    date_sql = """
                UPDATE
                    main
                SET
                    last_updated = temp_date.latest_date::date
                FROM
                    temp_date
                WHERE
                    temp_date."tool_name" = main.tool_name
                    AND temp_date.latest_date IS NOT NULL
          """

    language_sql = """
        UPDATE
            programming_language
        SET
            language = temp_lang.languages
        FROM
            temp_lang
        WHERE
            temp_lang."tool_name" = programming_language.tool_name
            AND temp_lang.languages IS NOT NULL
    """

    with engine.begin() as conn:
        conn.execute(date_sql)
        conn.execute(language_sql)
