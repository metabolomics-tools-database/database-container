from setuptools import setup, find_packages

setup(
    name="GitHub_Ext",
    version="0.0.1",
    author="Erik Serrano",
    author_email="erik.serrano@cuanshutz.edu",
    description=("Github API extension for MSCAT database"),
    packages=find_packages(),
    scripts=["./scripts/update_mscat.py", "./scripts/setup_key.py"],
    install_requires=[
        "requests",
        "python-dotenv",
        "validators",
        "perceval",
        "lxml",
        "html5lib",
    ],
)
