from typing import NamedTuple


class RepoStruct(NamedTuple):
    repo_id: str
    name: str
    full_name: str
    homepage: str
    isprivate: bool
    isfork: bool
    owner_name: str
    owner_url: str
    owner_repos: str
    commits_url: str
    branch: str
    topics: list
    description: str
    issues_url: str
    lang_url: str
    pulls_url: str
    git_url: str
    clone_url: str
    size: int
    stargazers_count: int
    watchers_count: int
    network_count: int
    subs_count: int
    n_forks: int
    n_issues: int
    top_lang: str
    has_issues: bool
    has_project: bool
    has_wiki: bool
    license_type: str
    license_node_id: str


class RepoSearchStruct(NamedTuple):
    owner: str
    name: str
    url: str
    description: str
    language: str
