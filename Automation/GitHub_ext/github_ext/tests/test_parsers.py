import unittest
import json
from github_ext.parsers.json_parser import parse_json_commit


class ParserTesting(unittest.TestCase):
    def test_single_commit(self):
        json_path = "./testdata/single_commit.json"
        reader = open(json_path, "r")
        content = json.load(reader)
        type_check = "<class 'github_ext.structs.commits.CommitStruct'>"
        struct = parse_json_commit(content)
        test_type = str(type(struct))
        reader.close()
        self.assertEqual(test_type, type_check)

    def test_langs_parser(self):
        """Tests if top language and can obtained all langauges"""
        datacheck1 = ("JavaScript", 98.499)
        datacheck2 = [
            ("JavaScript", 98.499),
            ("C++", 0.742),
            ("TypeScript", 0.348),
            ("CoffeeScript", 0.281),
            ("C", 0.088),
            ("Shell", 0.039),
            ("Python", 0.004),
        ]


def json_file_parser(path):
    # opening file and reading
    reader = open(path, "r")
    data = json.load(reader)
    return data
