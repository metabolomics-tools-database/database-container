import unittest
from time import sleep
from github_ext.common import errors
from github_ext.apis.github import GitHubClient


class TestGithubRequests(unittest.TestCase):
    """Tests the functionality of GitHubClient class"""

    def test_loading_githubkey(self):
        """Testing new githubkey loader function function is
        called when initalizing the GitHubClient()"""
        owner_name = "BenLangmead"
        repo_name = "bowtie2"
        git = GitHubClient(owner=owner_name, repo_name=repo_name)

        repr_check = "GitHubClient(owner={}, repo={}, branch={})".format(
            owner_name, repo_name, git.repo.branch
        )
        test_check = repr(git)

        self.assertEqual(test_check, repr_check)

    def test_onwer_exists_true(self):
        """Tests a sucessful requests with valid owner and repo name"""
        owner_name = "Facebook"
        repo_name = "react"
        caller = GitHubClient(owner=owner_name, repo_name=repo_name)

        repr_check = "GitHubClient(owner={}, repo={}, branch={})".format(
            owner_name, repo_name, caller.repo.branch
        )
        sleep(0.3)
        self.assertEqual(repr(caller), repr_check)

    def test_owner_exists_false(self):
        """Testing exceptions when invalid owner and/or repo is porivded"""
        owner_name = "FakeOwnerThatDoesNotExists"
        repo_name = "FakeRepositoryThatDoesNotExist"
        try:
            caller = GitHubClient(owner=owner_name, repo_name=repo_name)
        except errors.GithubRequestFailed:
            sleep(0.3)
            self.assertRaises(errors.GithubRequestFailed)

    def test_repo_attribute(self):
        """Tests if the RepoStruct is preloaded when initializing the
        GitHubClient"""
        owner_name = "Facebook"
        repo_name = "react"
        caller = GitHubClient(owner=owner_name, repo_name=repo_name)
        type_check = "<class 'github_ext.structs.repo.RepoStruct'>"
        sleep(0.3)
        self.assertEqual(str(type(caller.repo)), type_check)

    def test_markdown_request(self):
        owner_name = "Facebook"
        repo_name = "react"
        caller = GitHubClient(owner=owner_name, repo_name=repo_name)
        markdown_text = caller.get_readme_text()
        sleep(0.3)
        self.assertEqual(type(markdown_text), str)

    def test_commit_request(self):
        """Should return a CommitStruct after parsing"""
        # initlalize GitHubClient
        owner_name = "BenLangmead"
        repo_name = "bowtie2"
        shaid = "1c51df42184710144f4b2824fda8b98f691e704f"
        git = GitHubClient(owner=owner_name, repo_name=repo_name)

        # request for Commit JSON -> parser -> Returns CommitStruct
        struct = git.get_commit(sha_id=shaid)
        test_type = str(type(struct))
        type_check = "<class 'github_ext.structs.commits.CommitStruct'>"
        sleep(0.3)
        self.assertEqual(test_type, type_check)

    def test_get_total_comits(self):
        owner_name = "BenLangmead"
        repo_name = "bowtie2"
        git = GitHubClient(owner=owner_name, repo_name=repo_name)
        n_commits = git.get_total_commits()

        self.assertEqual(type(n_commits), int)

    @unittest.skip("Skipping test_multi_commit_requests. due to high level requests")
    def test_multi_commit_request(self):
        """Should return a CommitStruct after parsing"""
        # initlalize GitHubClient
        owner_name = "BenLangmead"
        repo_name = "bowtie2"
        shaid = "1c51df42184710144f4b2824fda8b98f691e704f"
        git = GitHubClient(owner=owner_name, repo_name=repo_name, buffer=0.5)

        # request for Commit JSON -> parser -> Returns CommitStruct
        list_of_structs = git.get_commits()
        type_check = "<class 'github_ext.structs.commits.CommitStruct'>"
        test_type = str(type(list_of_structs[0]))
        self.assertEqual(test_type, type_check)

    # TODO: Test is failing (recorded on project tasks under 'bugs')
    def test_lang_requests(self):
        """Tests if it can filter langauges and obtained the best languages"""
        datacheck1 = ("JavaScript", 98.499)
        datacheck2 = [
            ("JavaScript", 98.499),
            ("C++", 0.742),
            ("TypeScript", 0.348),
            ("CoffeeScript", 0.281),
            ("C", 0.088),
            ("Shell", 0.039),
            ("Python", 0.004),
        ]
        owner_name = "Facebook"
        repo_name = "react"
        git = GitHubClient(owner=owner_name, repo_name=repo_name, buffer=0.5)
        top_lang = git.get_top_language()
        all_lang = git.get_languages()
        self.assertEqual(datacheck1, top_lang)
        self.assertEqual(datacheck2, all_lang)
