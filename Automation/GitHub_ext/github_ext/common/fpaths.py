import os


class BaseRootPath:
    def __init__(self):
        self._get_root_path()
        root_path = None

    def _get_root_path(self):
        splitter = "database-container"
        header_path = os.getcwd().split(splitter)[0]
        root_path = os.path.join(header_path, splitter)
        self.root_path = root_path


class DataPaths(BaseRootPath):
    """Class that contains paths pointing to data files produced by mscat or
    and analytical scripts

    Parameters
    ----------
    BaseRootPath : class
        class that contain base paths
    """

    def __init__(self):
        super(DataPaths, self).__init__()
        self.mscat_root = self.root_path.rsplit("/", 1)[0]
        self.mscat_data = os.path.join(self.mscat_root, "csvDump.csv")


class ConfigPaths(BaseRootPath):
    def __init__(self):
        super(ConfigPaths, self).__init__()
        self.config_path = os.path.join(self.root_path, "configs")
        self.unsupported_lang_config_path = os.path.join(
            self.config_path, "not_supported_langs.json"
        )
