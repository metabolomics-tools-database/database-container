# -------------------------------
# biocon_parser.py
# used for parsing and extracting data within the bioconductor
# package web page.
# -------------------------------
import os
import requests
import validators
import numpy as np
from bs4 import BeautifulSoup


# github_ext imports
from github_ext.apis.url_patterns import UrlPatterns
from github_ext.common.checks import check_request


def get_license(url: str) -> str:
    """Parses Bioconductor package HTML page and extracts
    the license type.

    Parameters
    ----------
    url : url to bioconductor package

    Returns:
    --------
    str
        license type
    """
    rows = _get_all_rows(url)

    for row in rows:
        table_data = [data.text for data in row.find_all("td")]
        if len(table_data) == 0:
            continue
        elif table_data[0] == "License":
            license_type = table_data[1]
            return license_type


def get_bioconductor_tar_source(link):
    """gets git url from bioconductor webpage

    Parameters
    ----------
    link : str
        Link to bioconductor packages
    """
    try:
        html = requests.get(link, timeout=10.0).text
    except:
        return np.nan

    # parsing HTML
    pattern = UrlPatterns()
    soup = BeautifulSoup(html)

    tar_loc = soup.find("td", {"class": "rpack"})
    if tar_loc is None:
        return np.nan

    tar_source_endpoint = tar_loc.find("a").get("href").strip("../")
    tar_link = os.path.join(pattern.bioconductor_source, tar_source_endpoint)

    ## validate link
    if not validators.url(tar_link):
        raise RuntimeError("Invalid link constucted")

    return tar_link


def _get_all_rows(url: str) -> list:
    """Bioconductor has tables that stores metadata in their package
    page. This function parses HTML and obtains all <tr> elements

    Parameters
    ----------
    url : str
        url to bioconductor package

    Returns
    -------
    list
        list of bs4.element.ResultSet elements that points
        HTML table row element data
    """

    pattern = UrlPatterns()
    if not validators.url(url):
        raise RuntimeError("Invalid URL")
    elif not pattern.bioconductor_package in url:
        raise RuntimeError("Url provided does not point to a package website")

    # requesting
    req = requests.get(url, timeout=10)
    check_request(req.status_code)

    # getting HTML and parsing it with beautiful soup
    # -- extracting based on classname
    soup = BeautifulSoup(req.text, "html.parser")
    rows = soup.find_all("tr")
    return rows
