# -------------------------------
# github.py
# contains a class that is responsible for handling
# API calls to github
# -------------------------------
import os
import requests
import time

import numpy as np
import validators

from github_ext.apis.token import load_api_key
from github_ext.common.checks import check_request
from github_ext.common.errors import (
    MissingGitHubParams,
    InvalidLinkError,
)
from github_ext.apis.urls import EndPointUrls
from github_ext.parsers.json_parser import *


class GitHubClient(EndPointUrls):
    def __init__(self, owner, repo_name, timeout=10, buffer=0.3):

        # inheriting url and url methods
        super().__init__(owner, repo_name)

        # loading key
        oauth_key = load_api_key()

        # init parameters
        self.key = {"Authorization": "Token " + oauth_key}
        self.owner = owner
        self.repo_name = repo_name
        self.buffer = buffer
        self.timeout = timeout
        self.repo = None

        # checking init
        self.__init_repo()

    def get_readme_text(self):
        """Obtains raw text data from repository README files"""
        if self.repo is None:
            raise MissingGitHubParams(
                ".get_readme_text() requires a repository name. You have provided None"
            )
        url = os.path.join(
            self.contents, self.owner, self.repo_name, self.repo.branch, "README.md"
        )

        # checking URL format
        url_check = validators.url(url)
        if not url_check:
            raise InvalidLinkError(
                "Invalid formatted link was provided: {}".format(url)
            )

        # sending request
        req = requests.get(url, timeout=self.timeout, headers=self.key)
        check_request(req.status_code)
        raw_md_conts = req.text
        return raw_md_conts

    # TODO: need to find a consensus pattern to text all licenses
    def get_description_text(self):
        """Obtains raw content from the DESCRIPTION file in github

        Returns
        -------
        str
            contents in side DESCRIPTION file

        Raises
        ------
        InvalidLinkError
            Raised when invalid url or unable to connect to server
        """
        if self.repo is None:
            raise MissingGitHubParams(
                ".get_readme_text() requires a repository name. You have provided None"
            )
        url = os.path.join(
            self.contents, self.owner, self.repo_name, self.repo.branch, "DESCRIPTION"
        )

        url_check = validators.url(url)
        if not url_check:
            raise InvalidLinkError(
                "Invalid formatted link was provided: {}".format(url)
            )

    def get_commit(self, sha_id):
        """obtains a single commit with a given sha_id

        Parameters
        ----------
        sha_id : str
            unique id to a specific commit

        Return
        ------
        CommitStructs
            Contains all the fields obtained from commit entry

        Raises
        ------
        InvalidLinkError
            Raised when invalid url or unable to connect to server
        """

        # generating url and checking format
        url = os.path.join(self.repo_url, self.owner, self.repo_name, "commits", sha_id)
        url_check = validators.url(url)
        if not url_check:
            raise InvalidLinkError(
                "Invalid formatted link was provided: {}".format(url)
            )

        # sending request
        req = requests.get(url, headers=self.key, timeout=self.timeout)
        check_request(req.status_code)

        # return list of CommitStructs here
        commit_content = req.json()
        struct = parse_json_commit(commit_content)
        return struct

    def get_commits(self):
        """Requests all commits stored in the github API

        Returns
        -------
        list
            list of CommitStructs

        Raises
        ------
        InvalidLinkError
            Raised when invalid formatted link is generated
        """

        # generating url and checking format
        url = os.path.join(self.repo_url, self.owner, self.repo_name, "commits")
        url_check = validators.url(url)
        if not url_check:
            raise InvalidLinkError(
                "Invalid formatted link was provided: {}".format(url)
            )

        # sending request
        req = requests.get(url, headers=self.key, timeout=self.timeout)
        check_request(req.status_code)

        # collecting contents (list of commits )
        commit_contents = req.json()

        # iterating contents and collecting data
        commit_structs = []
        for commit in commit_contents:
            req2 = requests.get(commit["url"], headers=self.key, timeout=self.timeout)
            check_request(req2.status_code)
            commit_cont = req2.json()
            struct = parse_json_commit(commit_cont)
            commit_structs.append(struct)
            time.sleep(self.buffer)

        return commit_structs

    def get_latest_commit(self):
        """Gets information of the latest commit every done

        Returns
        -------
        InvalidLinkError
            Raised when invalid formatted link is generated

        CommitStruct
           Contains all the fields obtained from commit entry
        """
        # generating url and checking format
        url = os.path.join(self.repo_url, self.owner, self.repo_name, "commits")
        url_check = validators.url(url)
        if not url_check:
            raise InvalidLinkError(
                "Invalid formatted link was provided: {}".format(url)
            )

        # sending requests and obtaining latest commit
        req = requests.get(url, headers=self.key, timeout=self.timeout)
        check_request(req.status_code)
        latest_commit_cont = req.json()[0]
        time.sleep(self.buffer)

        req2 = requests.get(
            latest_commit_cont["url"], headers=self.key, timeout=self.timeout
        )
        check_request(req2.status_code)
        time.sleep(self.buffer)

        # collecting data
        contents = req2.json()
        struct = parse_json_commit(contents)

        return struct

    def get_languages(self):
        """Obtains the mostly used programing languages in the repo. Languages
        like HTML, CSS, Markdown or any markdown languages will not be considered.
        Notebooks are not supported yet. This requires to extract what language
        is used in the notebook.

        Returns
        -------
        list
            list of tuples containing (lang, percent_score)

        Raises
        ------
        GithubRequestFailed
            Raised when invalid request or unable to connect to server
        """
        req = requests.get(self.repo.lang_url, timeout=self.timeout, headers=self.key)
        check_request(req.status_code)
        langs_content = req.json()
        time.sleep(self.buffer)

        lang_scores = parse_json_languages(langs_content)
        if len(lang_scores) == 0:
            return []
        return lang_scores

    def get_top_language(self):
        """Gets the top scoring language of the repo

        Returns
        -------
        Tuple
            contains (lang, percent_score)
        """
        lang_scores = self.get_languages()
        if len(lang_scores) == 0:
            return (np.nan, np.nan)
        top_lang = lang_scores[0]
        return top_lang

    def get_all_languages(self):
        """Obtains all languages and concatenates them in to a single string

        Returns
        -------
        str
            string of all programing languages
        """
        lang_score = self.get_languages()
        all_langs = [lang for lang, _ in lang_score]
        lang_str = " ".join(all_langs)
        return lang_str

    def get_total_commits(self):
        """Gets the total number of commits done in the repository

        Returns:
        --------
        int
            number of commits
        """

        # creating url and checking it
        params = {"per_page": 1}

        url_check = validators.url(self.commits_url)
        if not url_check:
            raise url_check

        n_commits = self._n_entries(self.commits_url)
        return n_commits

    def get_total_contributors(self):
        """Gets number of contributors in repo

        Returns
        -------
        int
            number of contributors

        Raises
        ------
        InvalidLinkError
            invalid link
        """
        if not validators.url(self.contributors):
            raise InvalidLinkError(
                "Invalid formatted link was provided: {}".format(self.contributors)
            )
        n_contributors = self._n_entries(self.contributors)
        return n_contributors

    def __repr__(self):
        rep = "GitHubClient(owner={}, repo={}, branch={})".format(
            self.owner, self.repo_name, self.repo.branch
        )
        return rep

    # ------------------------------
    # NOTE: PRIVATE FUNCTIONS:
    # ------------------------------
    def __init_repo(self):
        """checks if repo exists

        Returns
        -------
        bool
            True if it bases, False if it fails
        """
        url = os.path.join(self.repo_url, self.owner, self.repo_name)

        if not validators.url(url):
            raise InvalidLinkError("Invalid url format captured {}".format(url))
        response = requests.get(url, headers=self.key, timeout=self.timeout)
        check = check_request(response.status_code)
        if check:
            content = response.json()
            struct = parse_json_repo(content)
            self.repo = struct

    def _n_entries(self, url):
        """GitHub api uses pages to list number of items. However, we can set the settings that each
        page has one entry. This means that one page is one entry. Therefore, the total of a specific
        entry is equal to the number of pages."""

        params = {"per_page": 1}

        # sending request
        req = requests.get(url, params=params, headers=self.key, timeout=self.timeout)
        check_request(req.status_code)

        entries = req.links
        if len(entries) == 0:
            entries = len(req.json())
            return entries

        last_commit_link = req.links["last"]["url"]
        url_params_list = " ".join(last_commit_link.split("?")[-1].split("&")).split()
        param_dict = dict([param.split("=") for param in url_params_list])
        entries = int(param_dict["page"])

        return entries
