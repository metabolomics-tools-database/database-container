# ------------------------------
# url_patterns.py
# contains link patterns normally used for searching within
# text data
# ------------------------------
class UrlPatterns:
    def __init__(self):
        self.github_link = "https://github.com/"
        self.bioconductor_link = "https://bioconductor.org/package"
        self.bioconductor_git = "https://git.bioconductor.org/packages"
        self.bioconductor_package = "bioconductor.org/packages/"
        self.bioconductor_source = "https://bioconductor.org/packages/release/bioc"
