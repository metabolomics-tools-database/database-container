# -------------------------------------
# git.py
# API caller for obtaining meta data from git urls or files
# This requires a 3rd party package called Perceval
# -- source: https://github.com/chaoss/grimoirelab-perceval
# -------------------------------------
import os
from collections import Counter
from datetime import datetime
import shutil
from perceval.backends.core.git import Git


class GitClient:
    """Handles extracting all git"""

    def __init__(self, git_url, tmp_name="temp"):
        self.__index = -1
        self.git_url = git_url

        tmp_path = "./temp/{}.git".format(tmp_name)
        if os.path.exists(tmp_path):
            shutil.rmtree(tmp_path)

        self.tmp_path = tmp_path
        self.__repo_obj = Git(uri=self.git_url, gitpath=self.tmp_path)
        self.commit_data = None
        self.__load_commit_meta()

    # public methods
    def first_commit_date(self) -> str:
        """Obtains first commit date

        Returns
        -------
        str
            date of first commit of repo
        """
        date = self.__extract_date(self.commit_data[0])
        return date

    def latest_commit(self) -> dict:
        """Returns last commit meta data

        Returns
        -------
        dict
            Last commit meta data
        """
        return self.commit_data[-1]

    def latest_commit_date(self) -> str:
        """Obtains latest commit date

        Returns
        -------
        str
            latest commit date formated in YYYY-MM-DD
        """
        last_commit = self.commit_data[-1]
        date = self.__extract_date(last_commit)
        return date

    def n_contributors(self) -> int:
        """Obtains the number of contirbutors in the project

        Returns:
        --------
        int
            Number of developers contributed into the project
        """
        names = []
        for entry in self.commit_data:
            name = entry["data"]["Author"].split("<")[0]
            names.append(name)

        names = set(names)
        n_devs = len(names)

        return n_devs

    def dev_commits(self) -> dict:
        """Obtains all deverloper names and the number of commits
        submitted to the repo

        Returns:
        --------
        dict
            key value pairs of contributor name and number of commits

        """
        names = []
        for entry in self.commit_data:
            name = entry["data"]["Author"].split("<")[0]
            names.append(name)

        dev_commits = Counter(names)
        return dev_commits

    def total_commits(self):
        """Total of commits in repo

        Returns:
        --------
        int
            total of commits
        """
        return len(self.commit_data)

    def lang_composition(self):
        """Returns the language composition of the repo

        Returns
        -------
        dict
            returns language extension and percent composition as
            key value pairs

        """
        prog_files = []
        for entry in self.commit_data:
            files = entry["data"]["files"]
            for f in files:
                fname = f["file"]
                fname_check = f["file"].split(".")
                if len(fname_check) != 2:
                    continue
                else:
                    prog_files.append(fname)

        # getting all unique file names
        prog_files = set(prog_files)

        # now count for
        ext_list = [fname.split(".")[1] for fname in prog_files]
        ext_count = Counter(ext_list)
        total_ext = sum([n_files for n_files in ext_count.values()])
        lang_comp = {
            lang_ext: round(fcounts / total_ext, 2)
            for lang_ext, fcounts in ext_count.items()
        }
        return lang_comp

    def all_languages(self):
        """
        obtains all language obtained from different.
        """
        lang_comp = self.lang_composition()
        lang_list = list(lang_comp.keys())
        all_lang_str = " ".join([lang_list])
        return all_lang_str

    def top_lang(self):
        """Returns the top language

        Return:
        -------
        tuple
            A tuple containing language extention and its score
        """
        lang_comp = self.lang_composition()
        top_lang = max(lang_comp, key=lang_comp.get)
        return (top_lang, lang_comp[top_lang])

    def remove_cache(self):
        """Removes the temp file"""
        shutil.rmtree("./temp")

    # META data for class functionality (iterator and indexing added)
    # -- allow index slicing
    # private_methods setting up commit data
    def __load_commit_meta(self):
        self.commit_data = [entry for entry in self.__repo_obj.fetch()]
        # self.__remove_temp(self.temp_path)

    def __extract_date(self, commit):
        """extract date from commit"""
        datetime_list = commit["data"]["CommitDate"].split()
        date_str = " ".join(datetime_list[1:3] + [datetime_list[4]])
        date_obj = str(datetime.strptime(date_str, "%b %d %Y"))
        date = date_obj.split()[0]
        return date

    def __getitem__(self, subscript):
        if isinstance(subscript, slice):
            return list(self.commit_data.__getitem__(subscript))
        else:
            return self.commit_data.__getitem__(subscript)

    # -- allows object to be an iterator
    def __iter__(self):
        return self

    # -- index iteratoins
    def __next__(self):
        # index increments every times the __next__() is called in for loops
        self.__index += 1

        # if the index is larger or equal to the length of the list
        # -- stop iterations
        try:
            return self.commit_data[self.__index]
        except IndexError:
            raise StopIteration
