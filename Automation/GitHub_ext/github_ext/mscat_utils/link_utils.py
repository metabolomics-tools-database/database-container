import os

import requests
import validators
import numpy as np
from bs4 import BeautifulSoup


# github_ext imports
from github_ext.apis.url_patterns import UrlPatterns


def search_source_links(name, link):
    """gets

    Parameters
    ----------
    name: str
        name of the program
    link : str
        link to source

    Returns
    -------
    str
        tool github link
    """
    pattern = UrlPatterns()

    # If nan is passed through just return it back
    if link is np.nan:
        return np.nan

    # getting all github links in webpage
    gh_links = get_all_github_links(link)

    if gh_links is np.nan:
        return np.nan

    # searching github links
    elif len(gh_links) != 0:
        for gh_link in gh_links:
            if name.lower() in gh_link.lower():
                owner_repo = gh_link.split("/")
                if len(owner_repo) < 5:
                    return np.nan

                owner, repo = tuple(owner_repo[3:5])

                # checking url
                github_link = os.path.join(pattern.github_link, owner, repo)
                url_check = validators.url(github_link)
                if not url_check:
                    continue

                return github_link

    # if not git hub links were not found, then look at git
    elif len(gh_links) == 0:

        # getting git links
        if pattern.bioconductor_package in link:
            git_links = get_bioconductor_git_source(link)
        else:
            git_links = get_all_git_links(link)

        # checking git links and returning it
        if git_links is np.nan:
            return np.nan

        elif isinstance(git_links, str):
            if name.lower() in git_links.lower():
                return git_links

        elif len(git_links) != 0:
            for git_link in git_links:
                if git_link.lower().endswith(name.lower()):
                    return git_link

        else:
            return np.nan
    else:
        return np.nan


def get_all_github_links(url):
    """Gets all github links

    Parameters
    ----------
    html_conts : str
        HTML webpage contents
    """

    # parses html contents and searches for all link elements
    pattern = UrlPatterns()
    all_links = extract_all_links(url)

    if all_links is np.nan:
        return np.nan

    # iterating through all links and getting only github links
    all_gh_links = []
    for link in all_links:
        lower_url = link.lower()
        if lower_url.startswith(pattern.github_link):

            # checking url format
            url_check = validators.url(link)
            if not url_check:
                continue

            # storing github links
            all_gh_links.append(link)

    return all_gh_links


def get_bioconductor_git_source(link):
    """gets git url from bioconductor webpage

    Parameters
    ----------
    link : str
        Link to bioconductor packages
    """
    try:
        html = requests.get(link, timeout=10.0).text
    except:
        return np.nan

    # parsing HTML
    pattern = UrlPatterns()
    soup = BeautifulSoup(html)

    tar_loc = soup.find("td", {"class": "rpack"})
    if tar_loc is None:
        return np.nan

    tar_source_endpoint = tar_loc.find("a").get("href").strip("../")
    tar_link = os.path.join(pattern.bioconductor_source, tar_source_endpoint)

    ## validate link
    if not validators.url(tar_link):
        raise RuntimeError("Invalid link constructed")

    return tar_link


def get_all_git_links(url):
    """obtains git urls from website

    Parameters
    ----------
    html_conts : str
        HTML webpage contents

    Returns
    -------
    list
        list of all git urls
    """
    pattern = UrlPatterns()
    links = extract_all_links(url)

    if links is np.nan:
        return np.nan

    # for link in links
    all_git_links = []
    for link in links:
        lower_url = link.lower()
        if lower_url.startswith(pattern.bioconductor_git):

            # checking url format
            url_check = validators.url(url)
            if not url_check:
                continue

            # storing github links
            all_git_links.append(url)

    return all_git_links


# HTML Parser module
def extract_all_links(url):
    """Extract all links in a webpage

    Parameters
    ----------
    url : str
        Link to web page

    Return
    ------
        list of all links in webpage
    """
    all_links = []

    # connecting to website
    # -- if the link does not work, return None
    try:
        r = requests.get(url, timeout=10.0).text
    except:
        # if any connection errors occurred return nan
        print("WARNING: unable to connect to {}".format(url))
        return np.nan

    # parses html contents and searches for all link elements
    soup = BeautifulSoup(r)
    link_elms = soup.findAll("a")

    if len(link_elms) == 0:
        return np.nan

    # iterating through every link and selecting github
    for link_elm in link_elms:

        # getting link text from link element
        href = link_elm.get("href")
        if href is None:
            continue

        # link format checking
        url_check = validators.url(href)
        if href is None or not url_check:
            continue

        all_links.append(href)

    return all_links
