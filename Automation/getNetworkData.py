import re
import pandas as pd
import psycopg2
from sqlalchemy import create_engine

# Connect to a database that exists
## Database connection settings (should be moved to envir variables)
engine = create_engine('postgresql+psycopg2://postgres:postgres@pgsql/postgres')

# Detect tools
def detect_tools(xmlDF, tool_vocabulary):
    detected_tools = []
    for i in range(xmlDF.shape[0]):
        row = xmlDF.iloc[[i]]
        PMID = row.PMIDs
        text = row.iloc[0, 1]
        for tool in tool_vocabulary.values:
            if re.search(r'(?:^|\W)'+tool[0]+r'(?:$|\W)', str(text)):
                detected_tools.append((tool[0],
                                    len(re.findall(r'(?:^|\W)'+tool[0]+r'(?:$|\W)', text)), # Number of times tool is on page
                                    PMID.values[0], 
                                    [s.start() for s in re.finditer(r'(?:^|\W)'+tool[0]+r'(?:$|\W)', text)][0] # Find position of tool
                                        ))
    return detected_tools

# Make df of detected tools
def make_detected_tools_df(detected_tools):
    df = pd.DataFrame(detected_tools, columns=["tool_name", "count", "PMID", "start_position"])
    return df.sort_values(by=["PMID", "start_position"]).reset_index(drop=True)

# Read mined XML and tool list
df = pd.read_csv("./minedXML.csv")
tools = pd.read_sql('SELECT tool_name FROM main;', engine)

detected_tools = detect_tools(df, tools)
detected_tools_df = make_detected_tools_df(detected_tools)

detected_tools_df.to_sql('network_visualization_data',con=engine, if_exists='replace')