import pandas as pd
import numpy as np
import json
from collections import Counter
import random
import sys
from joblib import dump, load

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag

import sklearn_crfsuite
from sklearn_crfsuite import metrics
from sklearn_crfsuite import scorers

import io
import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

class SentenceGetter(object):
    """
    Process the data to separate dataframe of tokens into sequences of sentences where each observation(sentence) will have its own predictors and labels
    """
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w,p,t) for w, p, t in zip(s['word'].values.tolist(),
                                                         s['POS'].values.tolist(),
                                                         s['Tag'].values.tolist())]
        self.grouped = self.data.groupby('Sentence #').apply(agg_func)
        self.sentences = [s for s in self.grouped]
    def get_next(self):
        try:
            s = self.grouped['Sentence: {}:'.format(self.n_sent)]
            self.n_sent += 1
            return s
        except:
            return None
# Functions that generate features/predictors for sequences in a sentence
def camel(word):
    return word != word.lower() and word != word.upper() and word.istitle() != True and "_" not in word
def word_features(sent, i):
        word = sent[i][0]
        postag = sent[i][1]
        
        features = {
            'bias': 1.0,
            'word': word,
            #'word[-3:]': word[-3:],
            #'word[-2:]': word[-2:],
            'word.isupper()': word.isupper(),
            'num_upper_chars': sum(map(str.isupper, word)),
            'camelCase': camel(word),
            'word.istitle()': word.istitle(),
            'word.isdigit()': word.isdigit(),
            'postag': postag,
            #'postag[:2]': postag[:2],
        }
        if i > 0:
            word1 = sent[i-1][0]
            postag1 = sent[i-1][1]
            features.update({
                '-1:word.lower()': word1.lower(),
                '-1:word.istitle()': word1.istitle(),
                '-1:word.isupper()': word1.isupper(),
                '-1:postag': postag1,
                #'-1:postag[:2]': postag1[:2],
            })
        else:
            features['BOS'] = True
        if i < len(sent)-1:
            word1 = sent[i+1][0]
            postag1 = sent[i+1][1]
            features.update({
                '+1word.lower()': word1.lower(),
                '+1word.istitle()': word1.istitle(),
                '+1word.isupper()': word1.isupper(),
                '+1postag': postag1,
                #'+1postag[:2]': postag1[:2],
            })
        else:
            features['EOS'] = True
        return features

def sent_features(sent):
    return [word_features(sent, i) for i in range(len(sent))]

def sent_labels(sent):
    return [label for token, postag, label in sent]

def sent_tokens(sent):
    return [token for token, postag, label in sent]
# Function to export csv to email
def export_csv(df):
  with io.StringIO() as buffer:
    df.to_csv(buffer)
    return buffer.getvalue()

try:
    noNewAbstracts = pd.read_csv("noNewAbstracts.csv")
except:
    print("New abstracts are available.")

if 'noNewAbstracts' in globals():
    sys.exit(0)
PubMedAbstracts = pd.read_csv("PubMedAbstracts.csv", usecols=['pmid', 'title_abstract'])
text = PubMedAbstracts.title_abstract
# process abstracts
text = text.str.cat()
text = word_tokenize(text)
text = pos_tag(text)
text = pd.DataFrame(text, columns=['word','POS']) # Tokens and part of speech
# Number sentences
n_sent = 1
sents = []
for word in text.word:
    if word == ".":
        sents.append("Sentence: {}".format(n_sent))
        n_sent += 1
    else:
        sents.append(np.nan)
text['Sentence #'] = sents
text['Sentence #'] = text['Sentence #'].bfill()
text['Tag'] = np.nan
# Get individual sentences and features
getter = SentenceGetter(text)
sentences = getter.sentences
X = [sent_features(s) for s in sentences]
# Load the conditional random fields model
crf = load('metabolomics_software_NLP_model.joblib')
# Use the model to make predictions
predictions = crf.predict(X)
# Extract the predicted tools
potential_tools = {}
for i in range(len(X)):
    for j in range(len(X[i])):
        name = X[i][j]['word']
        preds = predictions[i][j]
        if preds == 'T-name':
            potential_tools[name] = preds
# Check if tools are already in database or if they are already deemed as false positives
previousTools = None
try:
    previousTools = pd.read_csv("previousTools.csv")
except:
    print("No file found")
databaseTools = pd.read_csv("databaseTools.csv")
tools_output = pd.Series(list(potential_tools.keys()))
tools_output = pd.Series(tools_output.unique())
if previousTools is not None:
    previousTools = pd.concat([previousTools.squeeze(), databaseTools.squeeze()], ignore_index=True).drop_duplicates()
    newtools = tools_output[~tools_output.isin(previousTools)].dropna()
    previousTools = pd.concat([previousTools, newtools], ignore_index = True)
    previousTools.to_csv("previousTools.csv", encoding='utf-8', index=False)
    tools_output = newtools
else:
    tools_output.to_csv("previousTools.csv", encoding="utf-8", index=False)
# Send an email with the passed predictions
mail_content = '''Hello,
This is an automated email.
In this email we are sending potential metabolomics software tools.
'''
sender_address = 'MetaboGuru@gmail.com'
sender_pass = 'NLPmetabolomics'
receiver_address = 'MetaboGuru@gmail.com'
message = MIMEMultipart()
message['From'] = sender_address
message['To'] = receiver_address
message['Subject'] = 'Potential Metabolomics Software Tools.'

message.attach(MIMEText(mail_content, 'plain'))
attachment = MIMEApplication(export_csv(tools_output))
attachment['Content-Disposition'] = 'attachment; filename="{}"'.format('PotentialTools.csv')
message.attach(attachment)

session = smtplib.SMTP('smtp.gmail.com', 587) 
session.starttls() 
session.login(sender_address, sender_pass) 
text = message.as_string()
session.sendmail(sender_address, receiver_address, text)
session.quit()
print('eMail Sent.')