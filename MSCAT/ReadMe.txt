In order to produce the HTML landscape analysis:

    + Name the MSCAT data you wish to use csvDump.csv
    + Make sure csvDump.csv and Git_and_github_data.csv are in the same directory as Automated_Landscape_Analysis.Rmd
    + Install all required packages 
    + Open Automated_Landscape_Analysis.Rmd and on line 125 update 
        date_last_MSCAT_update <- as.Date("2022-04-14",'%Y-%m-%d') with the current date that matches the csvDump.csv data used/ the date on which you are running the file. 
    + Run Automated_Landscape_Analysis.Rmd using R 
