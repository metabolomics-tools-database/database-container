---
title: "MSCAT Landscape analysis"
output:
  html_document:
    df_print: paged
    toc: yes
    toc_depth: '6'
    toc_float: true
    number_sections: true
    code_folding: hide
---

**Data Processing code**

```{r, message=FALSE, echo=FALSE}
### Importing library
library(dplyr)
library(tidyverse)
library(splitstackshape)
library(stringr)
library(ggvenn)
library(here)
library(ggplot2)
library(wordcloud)
library(forcats)
library(plotly)
library(readr)
library(ggwordcloud)
library(tibble)
library(rcrossref)
library(corrplot)
library(data.table)
library("reticulate")
```

```{r, message=FALSE, echo=FALSE}
### Importing data
git_data <- read_csv("Git_and_github_data.csv")
data <- read.csv(here("csvDump.csv"))
my_blue <- "#26464e"
my_col_vec_1 <- c("#E6AB02","#795615","#549EC9", "#A92376","#FB9A99","#FF7F00","#0B070D", "#984EA3", "#E41A1C","#42F0ED","#8DD3C7","#DEC0F1","#FFED6F","#386CB0", "#F0027F", "#B75615","#666666","#A6D854","#7570B3","#66A61E", "#CCEBC5", "#A6CEE3", "#80B1D3", "#DECBE4", "#B2DF8A", "#F4CAE4", "#4DAF4A" ,"#E7298A" ,"#A65628", "#CAB2D6", "#FFED6F","#AE847E", "#EB60FB")
```

```{r, echo = FALSE}
#a function that takes a dataset and a column (containing csv) and returns the dataset with an a column for each value, encoded with 1 or 0, as well as a scores column: that computes the sum of all 1s
encode <- function(data, column) {
 data <- data %>%
  separate_rows(column, sep = ",")%>%
    mutate(value = 1)%>%
    mutate_all(na_if,"")%>%
    pivot_wider(names_from = column, values_from = value, values_fill = 0, values_fn = length)%>%
   select(-one_of("NA"))
 data$score <- rowSums(data[,6:length(data)])
  return(data)
}
```

```{python, echo = FALSE}
##Function that gets bibtex_list from DOI_list
import sys
import urllib.request
from urllib.error import HTTPError

def getbib(doi_list):
    bib_list = []
    BASE_URL = 'http://dx.doi.org/'
    for doi in doi_list:
        url = BASE_URL + doi
        req = urllib.request.Request(url)
        req.add_header('Accept', 'application/x-bibtex')
        try:
            with urllib.request.urlopen(req) as f:
                bibtex = f.read().decode()
                bib_list.append(bibtex)
        except HTTPError as e:
            if e.code == 404:
                bib_list.append("no bibtex")
                print('DOI not found.')
                
            else:
                print('Service unavailable.')
    return(bib_list)
  

```

```{r, echo=FALSE}
##function that extract publisher from bibtex
pub_extract <- function(bibtex) {
  curly_str <- str_extract(bibtex, "[\n\r][ \t]*publisher[ \t]*([^\n\r]*)") #extract journal with curly braces
  publisher <- gsub("publisher = ","",curly_str)
  publisher <- gsub("\\{","",publisher)
  publisher <- gsub ("\\}","", publisher)
  publisher <- gsub (",","", publisher)
  return(publisher)
}
```

```{r,echo=FALSE}
##function that extract journal from bibtex
jour_extract <- function(bibtex) {
  curly_str <- str_extract(bibtex, "[\n\r][ \t]*journal[ \t]*([^\n\r]*)") #extract journal with curly braces
  journal <- gsub("journal = ","",curly_str)
  journal <- gsub("\\{","",journal)
  journal <- gsub ("\\}","", journal)
  return(journal)
}
```

```{r, echo=FALSE}
#Function that convert journal name into abbreviations
jour_abbrev <- function(String) {
if (!is.na(String)){
  str_vector <- unlist(strsplit(String, " "))
  abb_string <- gsub(",","",toString(abbreviate(str_vector,4)))
  return(abb_string)
}
else return(String)
}
```

```{r, message= FALSE, warning= FALSE, results= FALSE}
### normalizing Citation by Software age
#Defining today's date, binding  Year.Published with initial dataset
date_last_MSCAT_update <- as.Date("2022-04-14",'%Y-%m-%d')


#Adding software age and number of days since last update onto the dataset, normalizing the data using software age
normalized_data<-  data%>%
  mutate(day_since_last_update = date_last_MSCAT_update - as.Date(Last.Updated, '%Y-%m-%d'))%>%
  mutate(software_age  = 2022 - Year.Published)%>%
  mutate(Normalized = trunc((Citations/software_age)*10,0))


# removing database only software and fit a linear regression to explore relationship between citation number and software age:
no_db <- data%>%
  mutate(day_since_last_update = date_last_MSCAT_update - as.Date(Last.Updated, '%Y-%m-%d'))%>%
  mutate(software_age  = 2022 - Year.Published)%>%
  filter(!str_detect("database", Functionality))

no_db_plot <- no_db%>%
  select(c(Tool.Name,software_age, Citations)) %>%
  filter(!is.na(Citations))%>%
  mutate(color_code = ifelse(Citations>500,"Citation > 500",ifelse(Citations<100, "Citation < 100","100 < Citation > 500")))
         
no_db_plot$color_code <- factor(no_db_plot$color_code, levels = c("Citation > 500", "100 < Citation > 500", "Citation < 100"))   
more_than_500 <- data%>%
  mutate(day_since_last_update = date_last_MSCAT_update - as.Date(Last.Updated, '%Y-%m-%d'))%>%
  mutate(software_age  = 2022 - Year.Published)%>%
  filter(!str_detect("database", Functionality))%>%
  filter(Citations > 500)

bet_100_500 <- data%>%
  mutate(day_since_last_update = date_last_MSCAT_update - as.Date(Last.Updated, '%Y-%m-%d'))%>%
  mutate(software_age  = 2022 - Year.Published)%>%
  filter(!str_detect("database", Functionality))%>%
  filter(Citations >= 100& Citations <=500)

less_than_100 <- data%>%
  mutate(day_since_last_update = date_last_MSCAT_update - as.Date(Last.Updated, '%Y-%m-%d'))%>%
  mutate(software_age  = 2022 - Year.Published)%>%
  filter(!str_detect("database", Functionality))%>%
  filter(Citations < 100)


### Dataset for normalized vs non normalized citations comparison
#Putting data set in a format that allows side by side comparison of normalized and non normalized citations 
reshaped_data<-normalized_data%>%
  select(-Website, -Last.Updated, -License)
reshaped_data <- gather(reshaped_data, key = output, value = n, Citations, Normalized, na.rm = FALSE, convert = FALSE)

## Update citation number and add doi as a variable
#Extract DOI from Publication link
papertable <- data %>%
  mutate(pub_doi = ifelse(data$Publications != "",str_extract(normalized_data$Publications, "10.\\d{4,9}/?%?[/%-._;()/:a-z0-9A-Z]+"),""))%>%
  mutate(pub_doi = gsub(")","",pub_doi))%>%
  select(c(1,20))

# Get citation count from Crossref
updated_cit <- papertable %>%
  filter(pub_doi != "")%>%
  mutate(pub_doi = str_replace_all(pub_doi,"%2F","/"))%>%
  mutate(Citations = cr_citation_count(pub_doi)$count)%>%
  group_by(Tool.Name) %>% 
    mutate(Citations = sum(Citations)) %>% 
    ungroup()

data <- data%>%
  select(-Citations)

data <- left_join(data,updated_cit)%>%
  select(c(1,2,3,4,20,5:19))

#Functionality
data_func<- reshaped_data%>%
  select(Tool.Name, output,n, Functionality,software_age, Year.Published)
#remove space to avoid duplicate 
data_func$Functionality <-  gsub(" ","",data_func$Functionality)
#Encode all data into large format and compute the number of functionality per tool 
data_func <- encode(data_func,"Functionality")


#There are some tools where the same functionalities where mentionned twice
#Detecting all duplicated functionalities
duplicate <- data_func%>%
  select (6:12)

#Correcting duplicate
duplicate[duplicate == 2]<-1
rowid_to_column(duplicate,"ID")
#Toolname info
toolname <- data.frame(data_func[,1:5])
rowid_to_column(toolname,"ID")
#Reforming dataset
data_func<- cbind(toolname,duplicate)
#Adding score variable
data_func$score <- rowSums(data_func[,6:length(data_func)])

#Percentage of tools that offers a functionlaity accross all years

# retrieving original dataset (without normalized citation), with number and function for each tool name
Function_per_tool <- data_func%>%
  filter(output == "Citations")%>%
  select(1,5,6:12)

#convert all function count into numeric variables
Function_per_tool[,c(3:9)]<- apply(Function_per_tool[,c(3:9)], 2,            
                    function(x) as.numeric(as.character(x)))

#compute the sum of tools using each function accross all years 
total_func_count <- as.data.frame(colSums(Function_per_tool[,c(3:9)], na.rm=TRUE))
names(total_func_count) <- "count"

#Renaming column
total_func_count<- total_func_count%>%
  rownames_to_column("Function")

#computing percentage for all years and label them as all
percentage_Func_per_tool <- total_func_count%>%
  mutate(total=nrow(Function_per_tool), percent = round(count/nrow(Function_per_tool),3)*100)%>%
  mutate(Year.Published = "all")

# Percentage of tools that offers a functionality grouped by year

all_func_data <- percentage_Func_per_tool

# grouped <- Function_per_tool%>%
# group_by(as.factor(Year.Published))

year_list<- unique((data_func$Year.Published))

for (i in year_list){
  age_func_percentage <- Function_per_tool%>%
    filter(Year.Published == i)
  total_tool <- nrow(age_func_percentage)
  age_func_count <- as.data.frame(colSums(age_func_percentage[,c(3:9)], na.rm=TRUE))
  names(age_func_count) <- "count"
  total_age_func_count<-age_func_count%>%
    rownames_to_column("Function")%>%
  mutate(total=total_tool, 
         percent = round((count/total_tool),3)*100,
         Year.Published = i)
  all_func_data <- rbind(all_func_data, total_age_func_count)
}

set.seed(7623527) 

my_viz<- all_func_data%>%
    filter(Year.Published == "all", percent > 5)

#Programming Language
data_pro<- reshaped_data%>%
  select(Tool.Name, output,n, Programming.Language, software_age, Year.Published)
##remove space to avoid duplicate 
data_pro$Programming.Language <-  gsub(" ","",data_pro$Programming.Language)
##Encode all data
data_pro <- encode(data_pro,"Programming.Language")

#Percentage of tools that uses a programming language accross all years
# retrieving original dataset (without normalized citation), with number and type of programming used for each tool name

Language_per_tool <- data_pro%>%
  filter(output == "Citations")%>%
  select(1,5,6:26)

#convert all language count into numeric variables
Language_per_tool[,c(3:23)]<- apply(Language_per_tool[,c(3:23)], 2,            
                    function(x) as.numeric(as.character(x)))

Language_per_tool<- Language_per_tool%>%
  mutate(Javascript = Javascript + JavaScript)%>%
  select(-JavaScript)

#compute the sum of tools using each languages accross all years 
total_count <- as.data.frame(colSums(Language_per_tool[,c(3:22)], na.rm=TRUE))
names(total_count) <- "count"

#Renaming column
total_count<- total_count%>%
  rownames_to_column("Language")

#computing percentage for all years and lable them as all
percentage_per_tool <- total_count%>%
  mutate(total=nrow(Language_per_tool),percent = round(count/nrow(Language_per_tool),3)*100)%>%
  mutate(Year.Published = "all")


#146 tools with their programming langugae table from git

language_git <- git_data%>%
  select(c("Tool Name","top_lang"))

#language 138 then 129
language_git <- language_git %>%
  filter(!is.na(top_lang))%>%
  filter(top_lang != "Rd")


language_git$top_lang[language_git$top_lang == "Vue"] <- "JavaScript"

git_viz <- language_git%>%
  group_by(top_lang)%>%
  count()%>%
  mutate(percent = round(n/nrow(language_git)*100,2))


top_only <- language_git%>%
  mutate(value = 0)%>%
  pivot_wider(names_from = top_lang, values_from = value, values_fill = 0, values_fn = length)%>%
  rename(Tool.Name = `Tool Name`)

my_tool <- language_git$`Tool Name`

#all tools with their functionality data table
all_func <- data_func%>%
  filter(output == "Citations")%>%
  select(1,3,6:13)%>%
  rename(score_func = score)

func_cit_analysis <- all_func%>%
  filter(!is.na(n))%>%
  filter(n!= 0)

top_func <- all_func[all_func$Tool.Name %in% my_tool,c(1:9)]

git_func <- left_join(top_func,top_only)

cor_dat <- cor(git_func[,c(3:20)])

shorter <- cor_dat[c(1:7),c(8:18)]

#Combination langugae, functionality
all_func <- data_func%>%
  filter(output == "Citations")%>%
  select(1,3,6:13)%>%
  rename(score_func = score)

func_cit_analysis <- all_func%>%
  filter(!is.na(n))%>%
  filter(n!= 0)

top_func_1 <- func_cit_analysis[func_cit_analysis$Tool.Name %in% my_tool,c(1:9)]

git_func_1 <- left_join(top_func_1,top_only)

# trial_1 <- git_func_1%>%
#   mutate(stat_Ana = statisticalanalysis*n)

my_col <- colnames(git_func_1[3:20])

for (i in 1:length(my_col)) {
git_func_1[,my_col[i]] <- git_func_1[,my_col[i]] *git_func_1$n
}
cor_dat_1 <- cor(git_func_1[,c(3:20)])

shorter_1 <- cor_dat_1[c(1:7),c(8:18)]


```

# What is MSCAT?

The Metabolomic software CATalogue (MSCAT) is a database of metabolomics software tools available online. MSCAT gets  updated by using Machine Learning methodologies to continuously identify new tools available from publications abstract.  MSCAT has been created to assist professionals working with metabolomic data in choosing the best fitted tool for their analysis.  

# Analysis

The goal of following landscape analysis is to get more insight on how and why professionals choose to use a software, which will be referred to as software popularity. breaks down the characteristics and usage of software tools based on the characteristics below:

* citation number extracted from the rcrossref package, 
* the software's functionality: The data processing step where the software is used (Pre/Post processing, Annotation, statitistical Analysis, and Database.
*Each functionality has a sub-list of methods. 
* Programming language: 
  * the programming languages that are available to the tool, 
  * Top programming language information extracted from git.

The MSCAT database currently contains information about `r nrow(data)` metabolomic software. The first software captured in MSCAT dates from the year `r min(data$Year.Published)` and the latest software is from the year `r max(data$Year.Published)`. 
Analysis were also run to detect relationship between citation number and journal of publication but no clear pattern were detected.


## Hypothesis: 

The analysis below respond to the following hypothesis:

Flexibility Hypothesis: Software popularity is related to the number of functionalities a software has

Specificity Hypothesis: Software popularity is related to specific combinations of functionalities and/or programming languages. 

## Using Citation number as a measure of software popularity

  a. Number of tools for Analysis
  
The popularity of a software is measured based on the number of papers that cited a tool’s original publication [and other papers related to the tool?] as per crossref. Citation number is extracted based on the doi of the tool’s original publication, therefore only tools that had a valid doi could be attributed a citation number. 

```{r, warning= FALSE, message= FALSE}
#Citation number descriptive:
summary(data$Citations)

```


From the original `r nrow(data)` number of software captured in MSCAT, `r length(data$Citations[!is.na(data$Citations)])`  papers have DOI that allowed citation numbers to be extracted. 

From the `r nrow(data)` number of tools, tools that are database were removed as they are not relevant for the analysis. Therefore the final number of tools we are working with is `r nrow(no_db_plot)`

  b.Citation normalization
  
The following graph looks at the relationship between Citation number and the number of years since a tool has been published, tool age.   

```{r, warning= FALSE, message= FALSE}
ggplot(no_db_plot, aes(x = software_age, y = Citations, color = color_code)) +
  geom_point()+
  geom_smooth(data = more_than_500, method = 'lm',se = FALSE, aes(x = software_age, y = Citations, colour="Citation > 500"),linetype="longdash",fullrange=TRUE)+
  geom_smooth(data = bet_100_500, method = 'lm',se = FALSE, aes(x = software_age, y = Citations, colour="100 < Citation > 500"),linetype="longdash", fullrange=TRUE)+
  geom_smooth(data = less_than_100, method = 'lm',se = FALSE, aes(x = software_age, y = Citations, colour="Citation < 100"),linetype="longdash")+
  scale_colour_manual(name="Legend: ", values=c("dark blue", "blue","light blue"))+
  theme_classic()+
  ggtitle("Citation VS Software Age") +
  xlab("Software Age")+
  ylab ("Citation number")

```
  
Tools whose publications have more than 500 citations, and less than 100 citations has a slightly positive relationship between the citation number or the age. Generally, the trend is still present.  It is expected that tools that have been available for a longer time have higher number of citations. For this reason, the citation number normalized by tool's age will be used as a measure of software popularity going forward.

### Comparing dataset normalized and not normalized
```{r, warning= FALSE, message= FALSE}
summary(normalized_data$Citations)
summary(normalized_data$Normalized)

```


## Landscape analysis
### FLEXIBILITY:
Hypothesis 1: Citation Number by Functionalities

One field used to describe the tools is Functionality. Functionality refers to the step in metabolomic data analysis where a tool is used. There are 5 functionalities depending on how the tool is used: processing, annotation, visualization/network, data management/workflow, statistical analysis.

```{R, warning= FALSE, message= FALSE}
my_viz %>%
    ggplot(aes(x = "", y = percent, fill = Function)) +
  geom_col() +
  geom_text(aes(label = percent),
            position = position_stack(vjust = 0.5),
            show.legend = FALSE) +
  scale_color_manual(values = sample(my_col_vec_1,10))+
  coord_polar(theta = "y") + 
  theme_void()+
    ggtitle("Percentage of Software that has one of the 7 functionalities \n from 1995-2021") 

```


Hypothesis 1 states that the more functionality a tool has the more it is used. 
Each point in the following graph represents a tool, and the x-axis shows how many functionality does the tool have while the y-axis represents its normalized citation number. 

```{r, warning= FALSE, message= FALSE}
data_func %>%
  filter(output == "Normalized")%>%
ggplot(aes(x = factor(score), y = n)) +
  geom_boxplot()+
   geom_point(colour = my_blue ) +
    ggtitle("Citation VS Functionality number") +
  xlab("Number of Functionality offered by each software")+
  ylab("Normalized Citation number")+
  theme_linedraw()+
    theme(axis.text=element_text(size=12),
        axis.title=element_text(size=12),
      plot.title = element_text(size=15, face = "bold"),
      axis.text.x = element_text(size=12),
      axis.text.y = element_text(size=12),
      strip.text = element_text(size=12))

```

<style>
div.blue { background-color:#e6f0ff; border-radius: 5px; padding: 10px;}
</style>
<div class = "blue">

There is no clear relationship between the number of functionality and the number of times a tool is cited
Majority of the tools (221/350) have a single functionality
Tools with 5 functionalities (6 tools) have higher citation number (median = 178), although they are way less frequent so there is not enough evidence that higher number of functionalities correlates with higher citation number
</div>

The specificity hypothesis dives deeper into the specific functionalities combination represented in the x-axis above. The second hypothesis is a qualitative analysis that looks at which functionalities are mostly combined together in a single tool and how many papers have cited the tools expressing such combination.

The second hypothesis also looks at the combination of functionality and programming language used by a tool as potential indicators of a tool's popularity. 

### SPECIFICITY

Hypothesis 2: Specific combination of functionalities/Programming languages

*Single functionality tools*

```{r, warning= FALSE, message= FALSE}
#softwares with a single functionalities
single_func_wider <- data_func%>%
  filter(output == "Normalized" &score == 1)

n_software <- nrow(single_func_wider)

single_longer_func <- data_func %>%
  select(1,6:12)%>%
  pivot_longer(!Tool.Name, names_to = "Functionalities", values_to = "count")
single_longer_func <- unique(single_longer_func)

my_single_data_info <- data_func%>%
  select(c(1:5,13))%>%
  filter(output == "Normalized" &score == 1)

full_longer_single <- left_join(single_longer_func,my_single_data_info)
full_longer_single <- unique(full_longer_single)

single_func_longer <- full_longer_single%>%
  filter(count == 1)%>%
  filter(score == 1)

frequency <- single_func_longer%>%
  group_by(Functionalities)%>%
  summarize(freq = sum(count),
            avg_cit = mean(n,na.rm = TRUE))%>%
  mutate(perc = round(freq/nrow(single_func_longer)*100))

```

Out of the `r nrow(data)`tools currently in MSCAT, `r sum(frequency$freq)`  have only one of the functionality above. The mostly cited tools among those are the one with `r frequency[frequency$avg_cit == max(frequency$avg_cit),1]$Functionalities` functionality followed by `r frequency[frequency$avg_cit == max(frequency$avg_cit[frequency$avg_cit != max(frequency$avg_cit)]),1]$Functionalities`. The functionality that is the most frequent in number is `r frequency[frequency$freq == max(frequency$freq),1]$Functionalities`followed by `r frequency[frequency$freq == max(frequency$freq[frequency$freq != max(frequency$freq)]),1]$Functionalities`.


```{r}
ggplot(frequency, aes(x = reorder(Functionalities,+avg_cit), y = round(avg_cit,0) , label = paste("n= ", freq,sep = ""), fill = Functionalities )) +
         geom_bar(stat = "identity") +
  geom_text(size=5,hjust=1.5)+
         coord_flip() + scale_y_continuous(name="Average normalized citation number for n tools") +
  scale_x_discrete(name = "") +
theme(axis.text.x = element_text(face="bold", color="#008000",size=24, angle=0),
          axis.text.y = element_text(face="bold", color="#008000", size=24, angle=0)) + 
  theme_classic(base_size = 15) + 
  guides(fill="none")
```


*Double*

```{R, warning= FALSE, message= FALSE}
double_func_wider <- data_func%>%
  filter(output == "Normalized" &score == 2)
nrow(double_func_wider)

double_longer_func <- data_func %>%
  select(1,6:12)%>%
  pivot_longer(!Tool.Name, names_to = "Functionalities", values_to = "count")
double_longer_func <- unique(double_longer_func)

my_double_data_info <- data_func%>%
  select(c(1:5,13))%>%
  filter(output == "Normalized" &score == 2)

full_longer_double<- left_join(double_longer_func,my_double_data_info)
full_longer_double <- unique(full_longer_double)

double_func_longer <- full_longer_double%>%
  filter(count == 1)%>%
  filter(score == 2)

#Prepping data for visualization

combination <- double_func_longer%>%
  mutate(combo = NA)

tool_list <- double_func_longer$Tool.Name
tool_seq <- seq(1,length(tool_list),2)

for (i in tool_seq ) {
  combination$combo[i] <- paste(double_func_longer$Functionalities[i],double_func_longer$Functionalities[i+1],sep = " & ")
}

combination <- (combination[,-c(2,3,4,6,7,8)])
n_software <- length(unique(tool_list))
combination <- combination[complete.cases(combination$combo), ]

combination_freq <-combination %>%
  group_by(combo)%>%
  count()

combination_cit <-combination %>%
  group_by(combo)%>%
  summarise(avg_Cit = round(mean(n, na.rm = TRUE)))

combination_freq <- inner_join(combination_freq,combination_cit)
combination_freq <- combination_freq[complete.cases(combination_freq),]

```

`r sum(combination_freq$n)` tools  have a combination of two functionality. The mostly cited tools are the one with `r combination_freq[combination_freq$avg_Cit == max(combination_freq$avg_Cit),1]$combo` functionalities followed by `r combination_freq[combination_freq$avg_Cit == max(combination_freq$avg_Cit[combination_freq$avg_Cit != max(combination_freq$avg_Cit)]),1]$combo`. The combination that is the most frequent in number is `r combination_freq[combination_freq$n == max(combination_freq$n),1]$combo` followed by `r combination_freq[combination_freq$n == max(combination_freq$n[combination_freq$n != max(combination_freq$n)]),1]$combo`.

```{R}
ggplot(combination_freq, aes(x = reorder(combo,+avg_Cit), y = avg_Cit , label = paste("n= ",n,sep = ""), fill = combo )) +
         geom_bar(stat = "identity") +
  geom_text(size=5,hjust=1.5)+
         coord_flip() + scale_y_continuous(name="Average normalized citation number out of n tools") +
  scale_x_discrete(name="Functionalities", ) +
theme(axis.text.x = element_text(face="bold", color="#008000",size=24, angle=0),
          axis.text.y = element_text(face="bold", color="#008000", size=24, angle=0)) + 
  theme_classic(base_size = 18)+
  guides(fill="none")
```

*Triple*

```{R, warning= FALSE, message= FALSE}
triple_func_wider <- data_func%>%
  filter(output == "Normalized" &score == 3)
nrow(triple_func_wider)

triple_longer_func <- data_func %>%
  select(1,6:12)%>%
  pivot_longer(!Tool.Name, names_to = "Functionalities", values_to = "count")
triple_longer_func <- unique(triple_longer_func)

my_triple_data_info <- data_func%>%
  select(c(1:5,13))%>%
  filter(output == "Normalized" &score == 3)

full_longer_triple<- left_join(triple_longer_func,my_triple_data_info)
full_longer_triple <- unique(full_longer_triple)

triple_func_longer <- full_longer_triple%>%
  filter(count == 1)%>%
  filter(score == 3)

#viz prep
combination <- triple_func_longer%>%
  mutate(combo = NA)

tool_list <- triple_func_longer$Tool.Name
n_software <- length(unique(tool_list))
tool_seq <- seq(1,length(tool_list),3)

for (i in tool_seq ) {
  combination$combo[i] <- paste(triple_func_longer$Functionalities[i],triple_func_longer$Functionalities[i+1], triple_func_longer$Functionalities[i+2],sep = " & ")
}


combination <- (combination[,-c(2,3,4,6,7,8)])

ORCA <- combination[61,]
combination <- combination[complete.cases(combination$combo), ]
combination <- rbind(combination, ORCA)

combination_freq <-combination %>%
  group_by(combo)%>%
  count()%>%
  mutate(perc = round(n/26*100))

combination_cit <-combination %>%
  group_by(combo)%>%
  summarise(avg_Cit = round(mean(n, na.rm = TRUE)))

combination_freq <- inner_join(combination_freq,combination_cit)
```

`r sum(combination_freq$n)` tools  have a combination of three functionality. The mostly cited tools are the one with `r combination_freq[combination_freq$avg_Cit == max(combination_freq$avg_Cit),1]$combo` functionalities followed by `r combination_freq[combination_freq$avg_Cit == max(combination_freq$avg_Cit[combination_freq$avg_Cit != max(combination_freq$avg_Cit)]),1]$combo`. The combination that is the most frequent in number is `r combination_freq[combination_freq$n == max(combination_freq$n),1]$combo`followed by `r combination_freq[combination_freq$n == max(combination_freq$n[combination_freq$n != max(combination_freq$n)]),1]$combo`.

```{R}
ggplot(combination_freq, aes(x = reorder(combo,+avg_Cit), y = avg_Cit , label = paste("n= ",n,sep = ""), fill = combo )) +
         geom_bar(stat = "identity") +
  geom_text(size=5,hjust=1.5)+
         coord_flip() + scale_y_continuous(name="Average normalized citation number out of n tools") +
  scale_x_discrete(name="Functionalities", ) +
theme(axis.text.x = element_text(face="bold", color="#008000",size=24, angle=0),
          axis.text.y = element_text(face="bold", color="#008000", size=24, angle=0)) + 
  theme_classic(base_size = 18)+
  guides(fill="none")
```

<style>
div.blue { background-color:#e6f0ff; border-radius: 5px; padding: 10px;}
</style>
<div class = "blue">

* Post-Processing, Pre-processing, Statistical Analysis and Annotation are the functionalities mostly observed in tools
* Annotation and Post-Processing are mostly mentioned in highly cited tools regardless of functionality numbers. 

</div>

*Programming language overview*

Another descriptive field in MSCAT is programming langugage which describes the languages that are available to the tools. This field has been populated by mining the publication paper of the tools. The bar graph below shows the percentage (above 1%) of tools that mention one of the below programming languages. Some tools mention more than one programming language.   

```{r, warning= FALSE, message= FALSE}
percentage_per_tool%>%
  filter(percent > 1)%>%
  ggplot(aes(x=Language, y=percent, fill=Language)) +
  geom_bar(stat="identity")+
  geom_text(aes(label=percent), vjust=-0.3, size=3.5)+
  theme_classic()+
    guides(fill="none")+
  ggtitle("Percentage above 1% of 350 tools that uses a Programming Language")+
  ylab("Percentage") + 
  xlab("Programming Language")
```
That said the mining of programming language from the publication paper only is not error proof, therefore the same information has been mined for `r nrow(language_git)` of these tools which are available on github. The graph below shows the proportion of tools and their top languge as shown on github repositories, that is it only shows one language per tool. 

```{r, warning= FALSE, message= FALSE}
ggplot(data=git_viz, aes(x=top_lang, y=percent, fill=top_lang)) +
  geom_bar(stat="identity")+
  geom_text(aes(label=percent), vjust=-0.3, size=3.5)+
  theme_classic()+
    guides(fill="none")+
  ggtitle(paste("Percentage of", nrow(language_git),"tools that mentions a Programming Language \n (extracted from GitHub) "))+
  ylab("Percentage") + 
  xlab("Programming Language")
```

*Functionality and Programming Languages*

The following graph shows a co-occurrence matrix of functionalities and programming languages in 129 tools (extracted from GitHub). A blue dot indicates functionalities and language that are frequently mentioned together, whereas a red dot indicate a combination that does not occur often. The bigger the dot, the more frequent the occurence and the smaller the less frequent. 

```{R, warning= FALSE, message= FALSE}

corrplot(shorter)

```
<style>
div.blue { background-color:#e6f0ff; border-radius: 5px; padding: 10px;}
</style>
<div class = "blue">

The following combinations are usually mentioned together:

* R and Statistical Analysis
* Python and Post-processing/Database
* JavaScript and Network/Visualization
* C++, Go, PHP and Data management workflow

The following combinations are rarely mentioned together:

* R and data management 
* C# and annotation
* JavaScript and Pre-processing 
* C++ and Statistical Analysis

</div>

*Functionality, Programming Languages and Citations*

The following matrix includes normalized citation number as a factor of the size of the dot. The larger the dot is, the higher the normalization citation of the paper that published a tool mentioning said programming language and functionality.   

```{r, warning= FALSE, message= FALSE}
corrplot(shorter_1)

```
<style>
div.blue { background-color:#e6f0ff; border-radius: 5px; padding: 10px;}
</style>
<div class = "blue">

The following combinations tend to be highly cited:
+ R and  Statistical Analysis / Post-processing
+ Python and NetworkViz/ Database

</div>
# Conclusion 

<style>
div.blue { background-color:#e6f0ff; border-radius: 5px; padding: 10px;}
</style>
<div class = "blue">

Software Popularity does not depend on flexibility, It depends on different set of features.
Highly cited tools often mention the following programming languages and/or functionalities:
R, Python, Annotation , Post-processing 
Tool citation number is not dependent on the journal the tool is published in.
The number of tools a journal publishes does not influence the number of times a tool is cited if published in the same journal.
</div>

##Journal analysis
```{r, eval = FALSE}
#Get a DOI character vector from normlized MSCAT data
doi_extract <- normalized_data %>%
  mutate(doi = ifelse(normalized_data$Publications != "",str_extract(normalized_data$Publications, "10.\\d{4,9}/?%?[/%-._;()/:a-z0-9A-Z]+"),0))%>%
  mutate(doi = gsub(")","",doi))%>%
  select(c(1,2,3,4,5,23,24))

doi_extract$doi_info <- "doi"
doi_extract$doi_info[is.na(doi_extract$doi)] <- "failed"
doi_extract$doi_info[doi_extract$doi == 0] <- "no_doi"
doi_extract$doi[doi_extract$doi == 0] <- NA


sum(doi_extract$doi_info == "failed")
sum(doi_extract$doi_info == "no_doi")

#DOI character vector with nas
doi_nas <-doi_extract$doi

#remove NAs
doi_vec<-doi_nas[!is.na(doi_nas)]


```
##Get bibtex metadata character vector from DOI character vector
```{python, eval = FALSE}
my_list = r.doi_vec ##import R data into python chunck
python_list = getbib(my_list)
```
##Extract publisher info from bibtex metadata and add onto a dataframe
```{r, eval = FALSE}
#Convert Character vector into a list
bibtex_chr <- py$python_list
bibtex_list <- as.list(bibtex_chr)
bibtex_list[[1]]

my_list<- c()
for (i in 1:length(bibtex_list)){
  my_list[i]<-bibtex_list[[i]]
}

list_fin <- list(my_list)
capture.output(summary(list_fin), file = "bibtex_list.txt")
write.table(as.data.frame(my_list),file="mylist.csv", quote=F,sep=",",row.names=F)

```
##Create a dataframe with doi and bibtex info
```{r, eval = FALSE}
doi_list <- as.list(doi_vec)
my_df <- setNames(data.frame(matrix(ncol = 2, nrow = length(doi_list))), c("doi","Bibtex"))
my_df$doi <- as.character(doi_list)
my_df$Bibtex <- bibtex_list
full_table <- left_join(doi_extract, my_df)
full_table <- unique(full_table)
```
##Extract publisher and journal presence vs absence info
```{r, eval = FALSE}
#Extract journal and publisher info and add as a new column
bibtex_df_1 <- full_table%>%
  mutate(publisher = pub_extract(Bibtex))%>%
  mutate(journal = jour_extract(Bibtex))%>%
  mutate(journal_info = "present")

bibtex_df_1$journal_info[bibtex_df_1$doi_info == "doi"&is.na(bibtex_df_1$journal)] <- "absent"
bibtex_df_1$journal_info[bibtex_df_1$doi_info != "doi" & is.na(bibtex_df_1$journal)] <- "no_doi"

sum(bibtex_df_1$journal_info == "absent")
sum(bibtex_df_1$journal_info == "no_doi")
```
