from ast import In
import pandas as pd
import numpy as np
import time
from functools import reduce
import itertools
import re
from db_process import process_data
from netGen_utils import *

import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import plotly.graph_objects as go
import plotly.express as px

# Database connection settings
db_params = "host=pgsql dbname=postgres user=postgres password=postgres"
# Tool tip function (this can be moved into its own script for cleanliness)


def create_tooltip(cell):
    num = len(cell)
    if num > 25:
        return(cell)
    else:
        return("")


markdownTable = """
| Meaning     |
| :---------- |
| Updated within 2 years    |
| Updated within 5 years |
| Updated within 10 years |
| Not updated within 10 years |
"""
# Get processed data
while True:
    try:
        (df_processing, df_statisticalanalysis, df_annotation, df_instrument, df_moltype, df_ui, df_language, df_license,
         df_os, counted, fig, user_df, today, formatted_two_Yago, formatted_five_Yago, formatted_ten_Yago, df_network) = process_data(db_params)
    except:
        continue
    break

# Generate the graph
# Read data
df_graph = df_network
# Generate graph properties
nodes, links = make_graph_data(df_graph)
G = make_network_plot_data(nodes, links)

# JavaScript and CSS style sheet
external_scripts = [
    'script.js', "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"]
external_stylesheets = ['styles.css']

### Application Layout ###

app = dash.Dash(__name__, external_scripts=external_scripts, external_stylesheets=external_stylesheets, meta_tags=[
    {"name": "viewport", "content": "width=device-width, initial-scale=1"}
])
app.title = "MSCAT"

tab_style = {
    'borderBottom': '1px solid #d6d6d6',
    'padding': '6px',
    'borderRadius': '10px 30px 0 0',
    'marginLeft': '-10px',
    'fontWeight': 'bold',
    'fontFamily': 'Arial'
}

tab_selected_style = {
    'borderTop': '1px solid #646464',
    'borderRight': '1px solid #646464',
    'borderBottom': '1px solid #646464',
    'borderLeft': '1px solid #646464',
    'backgroundColor': '#646464',
    'color': 'white',
    'fontWeight': 'bold',
    'fontFamily': 'Arial',
    'boxShadow': '10px 10px 8px #888888',
    'borderRadius': '10px 30px 0 0',
    'marginLeft': '-10px',
    'transition': '0.3s',
    'position': 'relative',
    'padding': '6px'
}
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='content', children=[
        html.H1(id='header', children="MSCAT: Metabolomics Software CATalog", style={
                "textAlign": "center"}),
        dcc.Tabs(id="tabs-styled-with-inline", value='tab-1', children=[
            dcc.Tab(label='Metabolomics Software Database', value='tab-1', style=tab_style, selected_style=tab_selected_style, id='tab1', children=[
                # dcc.RadioItems(
                #     id = 'radioitems',
                #     options=[{'label':'Curated Table', 'value':'static'}, {'label':'Editable table', 'value':'dynamic'}],
                #     value = 'static',
                #     labelStyle = dict(marginRight = '7px')
                #     ),
                # html.Div([html.P(id='editable-table-hidden', style={'display':'none'})]),

                dash_table.DataTable(
                    css=[{
                        'selector': '.dash-table-tooltip > table, .dash-table-tooltip > table th, .dash-table-tooltip > table td',
                        'rule': 'padding: 8px; text-align: left; border-bottom: 1px solid #ddd;'
                    },
                        {
                        'selector': '.dash-table-tooltip table',
                        'rule': 'border-collapse: collapse; width: 100%;'
                    },
                        {
                        'selector': '.dash-table-tooltip > table > tbody > tr:nth-child(1) > td',
                        'rule': 'background-color: white;'
                    },
                        {
                        'selector': '.dash-table-tooltip > table > tbody > tr:nth-child(2) > td',
                        'rule': 'background-color: #E0E0E0;'
                    },
                        {
                        'selector': '.dash-table-tooltip > table > tbody > tr:nth-child(3) > td',
                        'rule': 'background-color: #D3D3D3;'
                    },
                        {
                        'selector': '.dash-table-tooltip > table > tbody > tr:nth-child(4) > td',
                        'rule': 'background-color: #707070; color:white;'
                    }, ],
                    style_data_conditional=[
                        {
                            'if': {
                                'column_id': 'Last Updated',
                                'filter_query': '{}'.format('{Last Updated} <= ' + formatted_two_Yago)
                            },
                            'backgroundColor': '#E0E0E0',
                            'color': 'black',
                        },
                        {
                            'if': {
                                'column_id': 'Last Updated',
                                'filter_query': '{}'.format('{Last Updated} <= ' + formatted_five_Yago)
                            },
                            'backgroundColor': '#D3D3D3',
                            'color': 'black',
                        },
                        {
                            'if': {
                                'column_id': 'Last Updated',
                                'filter_query': '{}'.format('{Last Updated} <= ' + formatted_ten_Yago)
                            },
                            'backgroundColor': '#707070',
                            'color': 'white',
                        },
                        {
                            'if': {
                                'column_id': 'Tool Name'
                            },
                            'backgroundColor': '#F8F8F8',
                            'color': 'black',
                        },
                        {
                            'if': {
                                'column_id': 'Website'
                            },
                            'minWidth': '60px'
                        },
                        {
                            'if': {
                                'column_id': 'Publication'
                            },
                            'minWidth': '80px'
                        },
                        {
                            'if': {
                                'column_id': 'Last Updated'
                            },
                            'minWidth': '90px'
                        }],
                    id='table',
                    columns=[{'name': 'Tool Name', 'id': 'Tool Name', 'type': 'text', 'hideable': True},
                             {'name': 'Year Published', 'id': 'Year Published',
                                 'type': 'text', 'presentation': 'markdown', 'hideable': True},
                             {'name': 'Website', 'id': 'Website', 'type': 'text',
                                 'presentation': 'markdown', 'hideable': True},
                             {'name': 'Publications', 'id': 'Publications', 'type': 'text',
                              'presentation': 'markdown', 'hideable': True},
                             {'name': 'Citations', 'id': 'Citations',
                              'type': 'text', 'hideable': True},
                             {'name': 'Last Updated', 'id': 'Last Updated',
                              'type': 'datetime', 'hideable': True},
                             {'name': 'Operating System', 'id': 'OS',
                              'type': 'text', 'hideable': True},
                             {'name': 'User Interface', 'id': 'UI',
                              'type': 'text', 'hideable': True},
                             {'name': 'Molecule Type', 'id': 'Molecule Type',
                              'type': 'text', 'hideable': True},
                             {'name': 'Programming Language', 'id': 'Programming Language',
                              'type': 'text', 'hideable': True},
                             {'name': 'License', 'id': 'License',
                              'type': 'text', 'hideable': True},
                             {'name': 'Approach', 'id': 'Approach',
                              'type': 'text', 'hideable': True},
                             {'name': 'Functionality', 'id': 'Functionality',
                              'type': 'text', 'hideable': True},
                             {'name': 'Processing Methods', 'id': 'Processing',
                              'type': 'text', 'hideable': True},
                             {'name': 'Statistical Methods', 'id': 'Statistical Analysis',
                              'type': 'text', 'hideable': True},
                             {'name': 'Annotation Methods', 'id': 'Annotation',
                              'type': 'text', 'hideable': True},
                             {'name': 'Instrument Data Type', 'id': 'Instrument Data',
                              'type': 'text', 'hideable': True},
                             {'name': 'File Formats', 'id': 'File Formats',
                              'type': 'text', 'hideable': True}
                             #{'name':'Containers', 'id':'Containers', 'type':'text'},
                             #{'name':'Application', 'id':'Application', 'type':'text'}
                             ],
                    data=user_df.to_dict("records"),
                    page_size=1000,
                    tooltip_header={'Last Updated': {'value': markdownTable, 'type': 'markdown'},
                                    'Approach': 'The metabolomics approach (e.g. targeted, untargeted, imaging)'},
                    tooltip_data=[{column: {'value': create_tooltip(user_df.loc[i, column]), 'type': 'markdown'} for column in user_df.columns if column not in [
                        "Website", "Year Published", "Publication", "Last Updated", "Citations", "Publications"]} for i in range(len(user_df))],
                    tooltip_duration=None,
                    fixed_columns={'headers': True, 'data': 1},
                    #fixed_rows = {'headers':True, 'data':0},
                    style_header={'fontWeight': 'bold', 'border': 'thin lightgrey solid',
                                  'backgroundColor': 'rgb(100, 100, 100)', 'color': 'white'},
                    style_cell={'fontFamily': 'Arial', 'fontSize': '14px', 'textAlign': 'left', 'minWidth': '180px',
                                'maxWidth': '180px', 'whiteSpace': 'no-wrap', 'overflow': 'hidden', 'textOverflow': 'ellipsis'},
                    style_table={'overflowX': 'auto', 'overflowY': 'auto',
                                 'minWidth': '100%', 'maxHeight': '1000px'},
                    filter_action="native",
                    sort_action="native",
                    editable=False,
                    virtualization=True,
                    export_format="csv"
                )
            ]),
            dcc.Tab(label='Database Metrics', value='tab-2', style=tab_style, selected_style=tab_selected_style, id='tab2', children=[
                html.Div(id="Graphs-container", children=[
                    dcc.RadioItems(id='pieButtons', options=[{'label': 'Programming Language', 'value': 'lang'}, {'label': 'Software License', 'value': 'lic'}, {
                                   'label': 'Operating System', 'value': 'os'}, {'label': 'User Interface', 'value': 'ui'}], value='lang', labelStyle=dict(marginRight='7px')),
                    dcc.Graph(id="pieChart"),
                    dcc.Graph(id="sunburstChart", figure=px.sunburst(counted, path=[
                              'Function', 'Method'], values='Counts', title="Hierarchichal Visualization of Functionality")),
                    dcc.Graph(id="lineBarChart", figure=fig)
                ])
            ]),
            dcc.Tab(label='Workflow generator', value='tab-3', style=tab_style, selected_style=tab_selected_style, id='tab3', children=[
                html.Div(id="workflow-container", children=[
                    html.Datalist(id="proc_list"),
                    html.Datalist(id="stat_list"),
                    html.Datalist(id="ann_list"),
                    html.H2(id='inst_label', children="Instrument Type:"),
                    dcc.Dropdown(id='inst_type', options=[{'label': 'Any', 'value': 'any'}, {'label': 'MS', 'value': 'ms'}, {
                                 'label': 'NMR', 'value': 'nmr'}], value="any", style=dict(borderRadius="20px")),
                    html.H2(id='mol_label', children="Molecule Type:"),
                    dcc.Dropdown(id='mol_type', options=[{'label': 'Metabolomics', 'value': 'metabolomics'}, {'label': 'Lipidomics', 'value': 'lipidomics'}, {
                                 'label': 'Other Omics', 'value': 'genomics, proteomics, transcriptomics, glycomics'}], value="metabolomics", style=dict(borderRadius="20px")),
                    html.H2(id='ui_label', children="User Interface:",
                            style={"display": "inline-block"}),
                    dcc.Dropdown(id='user_int', options=[{'label': 'Any', 'value': 'any'}, {'label': 'GUI', 'value': 'gui'}, {
                                 'label': 'CLI', 'value': 'cli'}], value='any', style=dict(borderRadius="20px")),
                    html.Div(id='message', children=["This tab of the application allows you to generate potential workflows of metabolomics software tools. You select the methods that your pipeline consists of by using the dropdown selection menus above. You can filter these methods by instrument data type, the user interface, or by molecule type by using the dropdown menus above. In order to draw a Sankey workflow you must select at least two functionality fields"]),
                    # html.Div(id = "sankeyFields_container", children =[
                    dcc.Input(id="sank_proc", type="text",
                              placeholder="Processing method", list="proc_list"),
                    dcc.Input(id="sank_stat", type="text",
                              placeholder="Statistical analysis method", list="stat_list"),
                    dcc.Input(id="sank_ann", type="text",
                              placeholder="Annotation method", list="ann_list"),
                    html.Button(id="button", children=["Draw"]),
                    html.Button(id="buttonR", children=["Reset"]),
                    html.Div(id="Graph-container", children=[dcc.Graph(
                        id="Sankey-container")], style={'visibility': 'hidden'})
                    # ])

                ])
            ]),
            dcc.Tab(label='Co-occurence Network', value='tab-4', style=tab_style, selected_style=tab_selected_style, id='tab4', children=[
                html.Div(id="network-container", children=[
                    html.H2(id='node_include_names',
                            children="Include Nodes:"),
                    dcc.Dropdown(id='node_include_select', multi=True, options=[{'label': x, 'value': x} for x in np.sort(
                        G.vs['name'])], value=['MSPrep'], style=dict(borderRadius="20px")),
                    html.H2(id='node_exclude_names',
                            children="Exclude Nodes:"),
                    dcc.Dropdown(id='node_exclude_select',
                                 multi=True, style=dict(borderRadius="20px")),
                    html.H2(id='eWeight_filter',
                            children="Co-occurence \U00002265 :"),
                    dcc.Input(id='eWeight_select', type="number",
                              style=dict(borderRadius="20px")),
                    dcc.Loading(
                        id='loading-1',
                        children=[
                            html.Div(id="networkGraph-container", children=[dcc.Graph(
                                id="network_plot")], style={"display": "block", "width": "100%"})
                        ]
                    )

                ])
            ])


        ]), html.Footer(id='footer', children=[html.Div(id='info', children=["Database development supported by NIH Metabolomics Common Fund Program ", html.Br(),
                                                                             "U01 CA235488, U01CA235488-02S1 mPIs Katerina Kechris, Debashis Ghosh;", html.Br(),
                                                                             "Developers Wladimir Labeikovsky, Jonathan Dekermanjian", html.Br(),
                                                                             "Feedback: ", html.A(id='contact', style={'color': '#CFB97C'}, href="mailto:incoming+metabolomics-tools-database-database-container-24259151-issue-@incoming.gitlab.com", children=["MSCAT Service Desk"])]),
                                               ])
    ]),
])

### Application callbacks ###

# Can move some of these into own scripts for neatness


@app.callback(
    Output('pieChart', 'figure'),
    [Input('pieButtons', 'value')]
)
def pieChart(pieButtons):
    language_pie_data = df_language.groupby("language").agg(
        Counts=('language', 'count')).reset_index()
    language_pie_data.replace('', np.nan, inplace=True)
    language_pie_data.dropna(inplace=True)

    license_pie_data = df_license.groupby("license").agg(
        Counts=('license', 'count')).reset_index()
    license_pie_data.replace('', np.nan, inplace=True)
    license_pie_data.dropna(inplace=True)

    os_pie_data = df_os.groupby("os").agg(Counts=('os', 'count')).reset_index()
    os_pie_data.replace('', np.nan, inplace=True)
    os_pie_data.dropna(inplace=True)

    ui_pie_data = df_ui.groupby("ui").agg(Counts=('ui', 'count')).reset_index()
    ui_pie_data.replace('', np.nan, inplace=True)
    ui_pie_data.dropna(inplace=True)

    langlabels = list(language_pie_data.language)
    langvalues = list(language_pie_data.Counts)
    liclabels = list(license_pie_data.license)
    licvalues = list(license_pie_data.Counts)
    oslabels = list(os_pie_data.os)
    osvalues = list(os_pie_data.Counts)
    uilabels = list(ui_pie_data.ui)
    uivalues = list(ui_pie_data.Counts)

    langpie = go.Figure(data=[go.Pie(
        labels=langlabels, values=langvalues, name="Language", meta=['Language'])])
    langpie = langpie.update_traces(
        hovertemplate="%{meta[0]} = %{label}<br>Counts = %{value}<br>Percent = %{percent}", textinfo='label')
    langpie = langpie.update_layout(
        title_text="Programming Languages", xaxis_domain=[0.05, 1.0])
    licpie = go.Figure(data=[go.Pie(labels=liclabels, values=licvalues, name="License", meta=[
                       'License'], domain={'x': [0.0, 0.5], 'y':[0.2, 0.9]})])
    licpie = licpie.update_traces(
        hovertemplate="%{meta[0]} = %{label}<br>Counts = %{value}<br>Percent = %{percent}", textinfo='label')
    licpie = licpie.update_layout(
        title_text="Software Licenses", xaxis_domain=[0.05, 1.0])
    ospie = go.Figure(data=[go.Pie(
        labels=oslabels, values=osvalues, name="Operating System", meta=['OS'])])
    ospie = ospie.update_traces(
        hovertemplate="%{meta[0]} = %{label}<br>Counts = %{value}<br>Percent = %{percent}", textinfo='label')
    ospie = ospie.update_layout(
        title_text="Operating Systems", xaxis_domain=[0.05, 1.0])
    uipie = go.Figure(data=[go.Pie(
        labels=uilabels, values=uivalues, name="User Interface", meta=['UI'])])
    uipie = uipie.update_traces(
        hovertemplate="%{meta[0]} = %{label}<br>Counts = %{value}<br>Percent = %{percent}", textinfo='label')
    uipie = uipie.update_layout(
        title_text="User Interfaces", xaxis_domain=[0.05, 1.0])
    if pieButtons == 'lang':
        return langpie
    if pieButtons == 'lic':
        return licpie
    if pieButtons == 'os':
        return ospie
    if pieButtons == 'ui':
        return uipie


@app.callback(
    [Output("proc_list", "children"),
     Output("stat_list", "children"),
     Output("ann_list", "children")],
    [Input("inst_type", "value"),
     Input("mol_type", "value"),
     Input("user_int", "value")]
)
def field_props(inst_type='value', mol_type='value', user_int='value'):
    if inst_type != 'any':
        inst_selection = df_instrument[df_instrument["instrument"].str.contains(
            inst_type.upper())]
    else:
        inst_selection = df_instrument
    if re.match(r".*,", mol_type):
        mol_type = mol_type.split(", ")
        mol_selection = df_moltype[df_moltype["moltype"].isin(mol_type)]
    else:
        mol_selection = df_moltype[df_moltype["moltype"].isin([mol_type])]
    if user_int != "any":
        if user_int == "gui":
            user_int = ["web", "GUI"]
        else:
            user_int = ["CLI"]
        ui_selection = df_ui[df_ui["ui"].isin(user_int)]
    else:
        ui_selection = df_ui
    selected_frames = [inst_selection, mol_selection, ui_selection]
    selected_merged = reduce(lambda left, right: pd.merge(
        left, right, on=["tool_name"], how="right"), selected_frames)
    selected_merged.dropna(inplace=True)

    # Filter methodologies
    proc_filt = df_processing[df_processing["tool_name"].isin(
        selected_merged["tool_name"])]
    stat_filt = df_statisticalanalysis[df_statisticalanalysis["tool_name"].isin(
        ui_selection["tool_name"])]
    ann_filt = df_annotation[df_annotation["tool_name"].isin(
        selected_merged["tool_name"])]

    proc_list = [html.Option(i) for i in np.unique(proc_filt['processing'])]
    stat_list = [html.Option(i) for i in np.unique(stat_filt['stats_method'])]
    ann_list = [html.Option(i) for i in np.unique(ann_filt['annotation'])]
    return([proc_list, stat_list, ann_list])


@app.callback(
    [Output("button", "style"),
     Output("buttonR", "style"),
     Output("message", "style")],
    [Input("sank_proc", "value"),
     Input("sank_stat", "value"),
     Input("sank_ann", "value")]
)
def button_style(sank_proc='value', sank_stat='value', sank_ann='value'):
    methods = [sank_proc, sank_stat, sank_ann]
    if sum(x is not None and x != '' for x in methods) < 1:
        return([{"backgroundColor": "grey", "color": "white", "cursor": "not-allowed", "border": "1px solid grey", "pointerEvents": "None"}, {"backgroundColor": "grey", "color": "white", "cursor": "not-allowed", "border": "1px solid grey", "pointerEvents": "None"}, None])
    if sum(x is not None and x != '' for x in methods) < 2:
        return([{"backgroundColor": "grey", "color": "white", "cursor": "not-allowed", "border": "1px solid grey", "pointerEvents": "None"}, None, None])
    else:
        return([None, None, {"display": 'none'}])


@app.callback(
    [Output('sank_proc', 'value'),
     Output('sank_stat', 'value'),
     Output('sank_ann', 'value')],
    [Input("buttonR", "n_clicks")],
)
def resetSankeyFields(n_clicks):
    return(["", "", ""])


@app.callback(
    Output('Sankey-container', 'figure'),
    [Input("button", "n_clicks")],
    [State("sank_proc", "value"),
     State("sank_stat", "value"),
     State("sank_ann", "value"),
     State("inst_type", "value"),
     State("mol_type", "value"),
     State("user_int", "value")]
)
def sankey(n_clicks, sank_proc='value', sank_stat='value', sank_ann='value', inst_type='value', mol_type='value', user_int='value'):
    if n_clicks is None:
        raise PreventUpdate
    if inst_type != 'any':
        inst_selection = df_instrument[df_instrument["instrument"].str.contains(
            inst_type.upper())]
    else:
        inst_selection = df_instrument
    if re.match(r".*,", mol_type):
        mol_type = mol_type.split(", ")
        mol_selection = df_moltype[df_moltype["moltype"].isin(mol_type)]
    else:
        mol_selection = df_moltype[df_moltype["moltype"].isin([mol_type])]

    if user_int != "any":
        if user_int == "gui":
            user_int = ["web", "GUI"]
        else:
            user_int = ["CLI"]
        ui_selection = df_ui[df_ui["ui"].isin(user_int)]
    else:
        ui_selection = df_ui
    selected_frames = [inst_selection, mol_selection, ui_selection]
    selected_merged = reduce(lambda left, right: pd.merge(
        left, right, on=["tool_name"], how="right"), selected_frames)
    selected_merged.dropna(inplace=True)

    # Filter methodologies
    proc_filt = df_processing[df_processing["tool_name"].isin(
        selected_merged["tool_name"])]
    stat_filt = df_statisticalanalysis[df_statisticalanalysis["tool_name"].isin(
        ui_selection["tool_name"])]
    ann_filt = df_annotation[df_annotation["tool_name"].isin(
        selected_merged["tool_name"])]

    if sank_proc == '':
        sank_proc = None
    if sank_stat == '':
        sank_stat = None
    if sank_ann == '':
        sank_ann = None

    proc_method = proc_filt[proc_filt["processing"].isin([sank_proc])]
    stat_method = stat_filt[stat_filt["stats_method"].isin([sank_stat])]
    ann_method = ann_filt[ann_filt["annotation"].isin([sank_ann])]

    # Generate labels
    proc_labels = [tool+"_proc" for tool in proc_method["tool_name"]]
    stat_labels = [tool+"_stat" for tool in stat_method["tool_name"]]
    ann_labels = [tool+"_ann" for tool in ann_method["tool_name"]]
    labels = proc_labels+stat_labels+ann_labels
    #####################################################################################
    # Generate sources
    if proc_labels and stat_labels and ann_labels:
        proc_source = [index for index in range(len(proc_labels))]
        stat_source = [index for index in range(
            len(proc_labels+stat_labels))]
        ann_source = [index for index in range(
            len(proc_labels+stat_labels+ann_labels))]

        stat_source = list(np.setdiff1d(stat_source, proc_source))
        ann_source = list(np.setdiff1d(
            ann_source, stat_source+proc_source))

        proc_source_reps = list(itertools.chain.from_iterable(
            itertools.repeat(x, len(stat_source)) for x in proc_source))
        stat_source_reps = list(itertools.chain.from_iterable(
            itertools.repeat(x, len(ann_source)) for x in stat_source))
        source = proc_source_reps + stat_source_reps

    # Generate targets
        proc_target = list(itertools.chain.from_iterable(
            itertools.repeat(stat_source, len(proc_source))))
        stat_target = list(itertools.chain.from_iterable(
            itertools.repeat(ann_source, len(stat_source))))
        target = proc_target + stat_target
    ######################################################################################
    # No proc
    # Generate sources
    if not proc_labels:
        ann_source = [index for index in range(
            len(stat_labels+ann_labels))]

        ann_source = list(np.setdiff1d(ann_source, stat_source))

        stat_source_reps = list(itertools.chain.from_iterable(
            itertools.repeat(x, len(ann_source)) for x in stat_source))
        source = stat_source_reps

    # Generate targets
        stat_target = list(itertools.chain.from_iterable(
            itertools.repeat(ann_source, len(stat_source))))
        target = stat_target
    ######################################################################################
    # no stat
    # Generate sources
    if not stat_labels:
        proc_source = [index for index in range(len(proc_labels))]
        ann_source = [index for index in range(
            len(proc_labels+ann_labels))]

        ann_source = list(np.setdiff1d(ann_source, proc_source))

        proc_source_reps = list(itertools.chain.from_iterable(
            itertools.repeat(x, len(ann_source)) for x in proc_source))
        source = proc_source_reps

    # Generate targets
        proc_target = list(itertools.chain.from_iterable(
            itertools.repeat(ann_source, len(proc_source))))
        target = proc_target
    ######################################################################################
    # no ann
    # Generate sources
    if not ann_labels:
        proc_source = [index for index in range(len(proc_labels))]
        stat_source = [index for index in range(
            len(proc_labels+stat_labels))]

        stat_source = list(np.setdiff1d(stat_source, proc_source))

        proc_source_reps = list(itertools.chain.from_iterable(
            itertools.repeat(x, len(stat_source)) for x in proc_source))
        source = proc_source_reps

    # Generate targets
        proc_target = list(itertools.chain.from_iterable(
            itertools.repeat(stat_source, len(proc_source))))
        target = proc_target
    ######################################################################################
    #####################################################################################
    # Generate values
    value = list(itertools.chain.from_iterable(
        itertools.repeat(x, len(source)) for x in [1]))

    # Define colors
    cols = {"_proc": "skyblue",
            "_stat": "rosybrown", "_ann": "plum"}
    color = [cols[key]
             for x in labels for key in re.findall("_proc|_stat|_ann", x)]

    # Clean up labels
    labels = [re.sub("_proc|_stat|_ann", "", x) for x in labels]
    # Plot sankey
    fig = go.Figure(data=[go.Sankey(
        node=dict(
            pad=15,
            thickness=20,
            line=dict(color="black", width=0.5),
            label=labels,
            color=color
        ),
        link=dict(
            source=source,
            target=target,
            value=value
        ),
        textfont=dict(size=14))])
    fig.update_layout(
        title_text="Workflow for the selected methods", font_size=10)
    return(fig)


@app.callback(
    [Output('network_plot', 'figure'), Output(
        'node_exclude_select', 'options')],
    [Input('node_include_select', 'value'),
     Input('node_exclude_select', 'value'),
     Input('eWeight_select', 'value')]
)
def generate_network(nodes_include='value', nodes_exclude='value', weight_filter='value'):

    options = [{'label': x, 'value': x} for x in np.sort(G.vs['name'])]
    if not nodes_include and not nodes_exclude and not weight_filter:
        fig = make_network_plot(G)

    if not nodes_include and not nodes_exclude and weight_filter:
        G_filt = filter_network(G, gt_edge_weight=weight_filter)
        fig = make_network_plot(G_filt)

    if nodes_include and not nodes_exclude and not weight_filter:
        G_filt = include_network(G, selected_nodes=nodes_include)
        options = [{'label': x, 'value': x}
                   for x in np.sort(G_filt.vs['name'])]
        fig = make_network_plot(G_filt, selected_nodes=nodes_include)

    if not nodes_include and nodes_exclude and not weight_filter:
        G_filt = filter_network(G, selected_nodes=nodes_exclude)
        fig = make_network_plot(G_filt)

    if not nodes_include and nodes_exclude and weight_filter:
        G_filt = filter_network(
            G, selected_nodes=nodes_exclude, gt_edge_weight=weight_filter)
        fig = make_network_plot(G_filt)

    if nodes_include and not nodes_exclude and weight_filter:
        G_filt = include_network(G, selected_nodes=nodes_include)
        options = [{'label': x, 'value': x}
                   for x in np.sort(G_filt.vs['name'])]
        G_filt = filter_network(G_filt, gt_edge_weight=weight_filter)
        fig = make_network_plot(G_filt, selected_nodes=nodes_include)

    if nodes_include and nodes_exclude and not weight_filter:
        G_filt = include_network(G, selected_nodes=nodes_include)
        options = [{'label': x, 'value': x}
                   for x in np.sort(G_filt.vs['name'])]
        G_filt = filter_network(G_filt, selected_nodes=nodes_exclude)
        fig = make_network_plot(G_filt, selected_nodes=nodes_include)

    if nodes_include and nodes_exclude and weight_filter:
        G_filt = include_network(G, selected_nodes=nodes_include)
        options = [{'label': x, 'value': x}
                   for x in np.sort(G_filt.vs['name'])]
        G_filt = filter_network(
            G_filt, selected_nodes=nodes_exclude, gt_edge_weight=weight_filter)
        fig = make_network_plot(G_filt, selected_nodes=nodes_include)

    return fig, options


@app.callback(
    Output('Graph-container', 'style'),
    [Input('button', 'n_clicks'),
     Input('buttonR', 'n_clicks')]
)
def hide_graph(button, buttonR):
    toggle = False
    change = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'button' in change:
        toggle = True
    if 'buttonR' in change:
        toggle = False
    if toggle is False:
        return {"visibility": "hidden"}
    else:
        time.sleep(0.6)
        return {"visibility": "visible"}


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8080, debug=True)
