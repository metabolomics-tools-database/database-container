@app.callback(
	Output('Sankey-container', 'figure'),
	[Input('table', 'active_cell')]
	)
def sankey_data(active_cell):
    if active_cell is None:
        raise PreventUpdate
    if active_cell['column_id'] != "tool_name":
        raise PreventUpdate
    tool_name = active_cell['row_id']
    data = []
    pre_df = hierarchichal_df[hierarchichal_df['Pre-Processing'].sum(axis=1) > 0]
    post_df = hierarchichal_df[hierarchichal_df['Post-Processing'].sum(axis=1) > 0]
    stat_df = hierarchichal_df[hierarchichal_df['Statistical Analysis'].sum(axis=1) > 0]
    annot_df = hierarchichal_df[hierarchichal_df['Annotation'].sum(axis=1) > 0]
    # The following code is for tools that have a pre-processing function
    if tool_name in pre_df[('Main', 'tool_name')].values:
        tools_index = []
        file_outs = []
        tool_name_dict = pre_df[pre_df[(
            'Main', 'tool_name')] == tool_name][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        tools_dict = post_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        for key, val in tool_name_dict[0].items():
            if val == 1:
                file_outs.append(key)
        interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_outs}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
        interop_tools = post_df[('Main', 'tool_name')].iloc[tools_index]
        data = [(tool_name+"_Pre", tool+"_Post")
                for tool in interop_tools.values]
        stat_set = set()
        for tool in interop_tools.values:
            tools_index = []
            file_outs = []
            tool_name_dict = post_df[post_df[(
                'Main', 'tool_name')] == tool][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            tools_dict = stat_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            for key, val in tool_name_dict[0].items():
                if val == 1:
                    file_outs.append(key)
            interoplist = []
            for i in range(len(tools_dict)):
                interop_tool = {key: tools_dict[i][key] for key in file_outs}
                interoplist.append(interop_tool)
                interKey = []
                for key, val in interop_tool.items():
                    if val == 1:
                        interKey.append(key)
                plang = []
                os = []
                intype = []
                mltype = []
                for i in range(len(interKey)):
                    if interKey[i][0] == "Programming Language":
                        plang.append(True)
                    else:
                        plang.append(False)
        
                    if interKey[i][0] == "OS":
                        os.append(True)
                    else:
                        os.append(False)
        
                    if interKey[i][0] == "Instrument Type":
                        intype.append(True)
                    else:
                        intype.append(False)
        
                    if interKey[i][0] == "Molecule Type":
                        mltype.append(True)
                    else:
                        mltype.append(False)
                if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                    tools_index.append(i)
                interop_tools_post = stat_df[(
                    'Main', 'tool_name')].iloc[tools_index]
            for j in interop_tools_post.values:
                stat_set.add(j)
            data_post = [(tool+"_Post", connect+"_Stat")
                            for connect in interop_tools_post.values]
            data = data + data_post
        annot_set = set()
        for tool in stat_set:
            tools_index = []
            file_outs = []
            tool_name_dict = stat_df[stat_df[(
                'Main', 'tool_name')] == tool][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            tools_dict = annot_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            for key, val in tool_name_dict[0].items():
                if val == 1:
                    file_outs.append(key)
            interoplist = []
            for i in range(len(tools_dict)):
                interop_tool = {key: tools_dict[i][key] for key in file_outs}
                interoplist.append(interop_tool)
                interKey = []
                for key, val in interop_tool.items():
                    if val == 1:
                        interKey.append(key)
                plang = []
                os = []
                intype = []
                mltype = []
                for i in range(len(interKey)):
                    if interKey[i][0] == "Programming Language":
                        plang.append(True)
                    else:
                        plang.append(False)
        
                    if interKey[i][0] == "OS":
                        os.append(True)
                    else:
                        os.append(False)
        
                    if interKey[i][0] == "Instrument Type":
                        intype.append(True)
                    else:
                        intype.append(False)
        
                    if interKey[i][0] == "Molecule Type":
                        mltype.append(True)
                    else:
                        mltype.append(False)
                if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                    tools_index.append(i)
                interop_tools_stat = annot_df[(
                    'Main', 'tool_name')].iloc[tools_index]
            for j in interop_tools_stat.values:
                annot_set.add(j+"_Annot")
            data_stat = [(tool+"_Stat", connect+"_Annot")
                            for connect in interop_tools_stat.values]
            data = data + data_stat
            seen = set()
            data = [t for t in data if tuple(sorted(t)) not in seen and not seen.add(tuple(sorted(t)))]
        label = []
        for i in range(len(data)):
            label.append(data[i][0])
            label.append(data[i][1])
        label = label + list(annot_set)
        #label_set = set(label)

        uniq_label = []
        for i in label:
            if i not in uniq_label:
                uniq_label.append(i)
        target_source_dict = {name: cat for name, cat in list(
            zip(uniq_label, range(0, len(uniq_label))))}
        target = []
        source = []
        value = []
        for i in range(len(data)):
            source.append(target_source_dict[data[i][0]])
            target.append(target_source_dict[data[i][1]])
            value.append(1)

        # Create the visual
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=uniq_label,

            ),
            link=dict(
                source=source,
                target=target,
                value=value
            ))])
        if data:
            fig.update_layout(
                title_text="Potential Pipe with" + " " + tool_name, font_size=10)
        else:
            fig.update_layout(
                title_text="No pipeline found for" + " " + tool_name, font_size=10)
        return fig
    elif tool_name in post_df[('Main', 'tool_name')].values:
        # Need to map backwards to find pre-processing tools that can feed into the desired tool
        tools_index = []
        file_ins = []
        tool_name_dict = post_df[post_df[(
            'Main', 'tool_name')] == tool_name][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        tools_dict = pre_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        for key, val in tool_name_dict[0].items():
            if val == 1:
                file_ins.append(key)
        interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_ins}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
        interop_tools = pre_df[('Main', 'tool_name')].iloc[tools_index]
        data = [(tool+"_Pre", tool_name+"_Post")
                for tool in interop_tools.values]
        stat_set = set()
        tools_index = []
        file_outs = []
        tool_name_dict = post_df[post_df[(
            'Main', 'tool_name')] == tool_name][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        tools_dict = stat_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        for key, val in tool_name_dict[0].items():
            if val == 1:
                file_outs.append(key)
        interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_outs}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
        interop_tools_post = stat_df[(
            'Main', 'tool_name')].iloc[tools_index]
        for j in interop_tools_post.values:
            stat_set.add(j)
        data_post = [(tool_name+"_Post", connect+"_Stat")
                        for connect in interop_tools_post.values]
        data = data + data_post

        annot_set = set()
        for tool in stat_set:
            tools_index = []
            file_outs = []
            tool_name_dict = stat_df[stat_df[(
                'Main', 'tool_name')] == tool][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            tools_dict = annot_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            for key, val in tool_name_dict[0].items():
                if val == 1:
                    file_outs.append(key)
            interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_outs}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
            interop_tools_post = annot_df[(
                'Main', 'tool_name')].iloc[tools_index]
            for j in interop_tools_post.values:
                annot_set.add(j+"_Annot")
            data_stat = [(tool+"_Stat", connect+"_Annot")
                            for connect in interop_tools_post.values]
            data = data + data_stat
            seen = set()
            data = [t for t in data if tuple(sorted(t)) not in seen and not seen.add(tuple(sorted(t)))]

        label = []
        for i in range(len(data)):
            label.append(data[i][0])
            label.append(data[i][1])
        label = label + list(annot_set)
        #label_set = set(label)

        uniq_label = []
        for i in label:
            if i not in uniq_label:
                uniq_label.append(i)
        target_source_dict = {name: cat for name, cat in list(
            zip(uniq_label, range(0, len(uniq_label))))}
        target = []
        source = []
        value = []
        for i in range(len(data)):
            source.append(target_source_dict[data[i][0]])
            target.append(target_source_dict[data[i][1]])
            value.append(1)

        # Create the visual
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=uniq_label,

            ),
            link=dict(
                source=source,
                target=target,
                value=value
            ))])
        if data:
            fig.update_layout(
                title_text="Potential Pipe with" + " " + tool_name, font_size=10)
        else:
            fig.update_layout(
                title_text="No pipeline found for" + " " + tool_name, font_size=10)
        return fig
    # Logic for an exclusively statistical analysis tool
    elif tool_name in stat_df[('Main', 'tool_name')].values:
        tools_index = []
        file_ins = []
        tool_name_dict = stat_df[stat_df[(
            'Main', 'tool_name')] == tool_name][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        tools_dict = post_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        for key, val in tool_name_dict[0].items():
            if val == 1:
                file_ins.append(key)
        interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_ins}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
        interop_tools = post_df[('Main', 'tool_name')].iloc[tools_index]
        data = [(tool+"_Post", tool_name+"_Stat")
                for tool in interop_tools.values]
        pre_set = set()
        for tool in interop_tools.values:
            tools_index = []
            file_ins = []
            tool_name_dict = post_df[post_df[(
                'Main', 'tool_name')] == tool][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            tools_dict = pre_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            for key, val in tool_name_dict[0].items():
                if val == 1:
                    file_ins.append(key)
            interoplist = []
            for i in range(len(tools_dict)):
                interop_tool = {key: tools_dict[i][key] for key in file_ins}
                interoplist.append(interop_tool)
                interKey = []
                for key, val in interop_tool.items():
                    if val == 1:
                        interKey.append(key)
                plang = []
                os = []
                intype = []
                mltype = []
                for i in range(len(interKey)):
                    if interKey[i][0] == "Programming Language":
                        plang.append(True)
                    else:
                        plang.append(False)
        
                    if interKey[i][0] == "OS":
                        os.append(True)
                    else:
                        os.append(False)
        
                    if interKey[i][0] == "Instrument Type":
                        intype.append(True)
                    else:
                        intype.append(False)
        
                    if interKey[i][0] == "Molecule Type":
                        mltype.append(True)
                    else:
                        mltype.append(False)
                if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                    tools_index.append(i)
            interop_tools_post = pre_df[(
                'Main', 'tool_name')].iloc[tools_index]
            for j in interop_tools_post.values:
                pre_set.add(j)
            data_post = [(connect+"_Pre", tool+"_Post")
                            for connect in interop_tools_post.values]
            data = data_post + data
            
        annot_set = set()
        
        tools_index = []
        file_outs = []
        tool_name_dict = stat_df[stat_df[(
            'Main', 'tool_name')] == tool_name][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        tools_dict = annot_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        for key, val in tool_name_dict[0].items():
            if val == 1:
                file_outs.append(key)
        interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_outs}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
        interop_tools_post = annot_df[(
            'Main', 'tool_name')].iloc[tools_index]
        for j in interop_tools_post.values:
            annot_set.add(j+"_Annot")
        data_stat = [(tool_name+"_Stat", connect+"_Annot")
                        for connect in interop_tools_post.values]
        data = data + data_stat
        seen = set()
        data = [t for t in data if tuple(sorted(t)) not in seen and not seen.add(tuple(sorted(t)))]

        label = []
        for i in range(len(data)):
            label.append(data[i][0])
            label.append(data[i][1])
        label = label + list(annot_set)
        label_set = set(label)

        uniq_label = []
        for i in label:
            if i not in uniq_label:
                uniq_label.append(i)
        target_source_dict = {name: cat for name, cat in list(
            zip(uniq_label, range(0, len(uniq_label))))}
        target = []
        source = []
        value = []
        for i in range(len(data)):
            source.append(target_source_dict[data[i][0]])
            target.append(target_source_dict[data[i][1]])
            value.append(1)

        # Create the visual
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=uniq_label,

            ),
            link=dict(
                source=source,
                target=target,
                value=value
            ))])
        if data:
            fig.update_layout(
                title_text="Potential Pipe with" + " " + tool_name, font_size=10)
        else:
            fig.update_layout(
                title_text="No pipeline found for" + " " + tool_name, font_size=10)
        return fig
# Logic for an exclusively annotation tool
    elif tool_name in annot_df[('Main', 'tool_name')].values:
        tools_index = []
        file_ins = []
        tool_name_dict = annot_df[annot_df[(
            'Main', 'tool_name')] == tool_name][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        tools_dict = stat_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
        for key, val in tool_name_dict[0].items():
            if val == 1:
                file_ins.append(key)
        interoplist = []
        for i in range(len(tools_dict)):
            interop_tool = {key: tools_dict[i][key] for key in file_ins}
            interoplist.append(interop_tool)
            interKey = []
            for key, val in interop_tool.items():
                if val == 1:
                    interKey.append(key)
            plang = []
            os = []
            intype = []
            mltype = []
            for i in range(len(interKey)):
                if interKey[i][0] == "Programming Language":
                    plang.append(True)
                else:
                    plang.append(False)
        
                if interKey[i][0] == "OS":
                    os.append(True)
                else:
                    os.append(False)
        
                if interKey[i][0] == "Instrument Type":
                    intype.append(True)
                else:
                    intype.append(False)
        
                if interKey[i][0] == "Molecule Type":
                    mltype.append(True)
                else:
                    mltype.append(False)
            if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                tools_index.append(i)
        interop_tools = stat_df[('Main', 'tool_name')].iloc[tools_index]
        data = [(tool+"_Stat", tool_name+"_Annot")
                for tool in interop_tools.values]
        
        annot_set = set()
        for i in range(len(data)):
            annot_set.add(data[i][1])
        post_set = set()
        for tool in interop_tools.values:
            tools_index = []
            file_ins = []
            tool_name_dict = stat_df[stat_df[(
                'Main', 'tool_name')] == tool][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            tools_dict = post_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            for key, val in tool_name_dict[0].items():
                if val == 1:
                    file_ins.append(key)
            interoplist = []
            for i in range(len(tools_dict)):
                interop_tool = {key: tools_dict[i][key] for key in file_ins}
                interoplist.append(interop_tool)
                interKey = []
                for key, val in interop_tool.items():
                    if val == 1:
                        interKey.append(key)
                plang = []
                os = []
                intype = []
                mltype = []
                for i in range(len(interKey)):
                    if interKey[i][0] == "Programming Language":
                        plang.append(True)
                    else:
                        plang.append(False)
        
                    if interKey[i][0] == "OS":
                        os.append(True)
                    else:
                        os.append(False)
        
                    if interKey[i][0] == "Instrument Type":
                        intype.append(True)
                    else:
                        intype.append(False)
        
                    if interKey[i][0] == "Molecule Type":
                        mltype.append(True)
                    else:
                        mltype.append(False)
                if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                    tools_index.append(i)
            interop_tools_post = post_df[(
                'Main', 'tool_name')].iloc[tools_index]
            for j in interop_tools_post.values:
                post_set.add(j)
            data_post = [(connect+"_Post", tool+"_Stat")
                            for connect in interop_tools_post.values]
            data = data_post + data
        
        pre_set = set()
        for tool in interop_tools.values:
            tools_index = []
            file_ins = []
            tool_name_dict = post_df[post_df[(
                'Main', 'tool_name')] == tool][['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            tools_dict = pre_df[['Programming Language', 'OS', 'Instrument Type', 'Molecule Type']].to_dict('records')
            
            if len(tool_name_dict) >0: # Add this handle everywhere
                for key, val in tool_name_dict[0].items():
                    if val == 1:
                        file_ins.append(key)
            interoplist = []
            for i in range(len(tools_dict)):
                interop_tool = {key: tools_dict[i][key] for key in file_ins}
                interoplist.append(interop_tool)
                interKey = []
                for key, val in interop_tool.items():
                    if val == 1:
                        interKey.append(key)
                plang = []
                os = []
                intype = []
                mltype = []
                for i in range(len(interKey)):
                    if interKey[i][0] == "Programming Language":
                        plang.append(True)
                    else:
                        plang.append(False)
        
                    if interKey[i][0] == "OS":
                        os.append(True)
                    else:
                        os.append(False)
        
                    if interKey[i][0] == "Instrument Type":
                        intype.append(True)
                    else:
                        intype.append(False)
        
                    if interKey[i][0] == "Molecule Type":
                        mltype.append(True)
                    else:
                        mltype.append(False)
                if len(interop_tool) > 0 and sum(plang) >=1 and sum(os) >= 1 and sum(intype) >= 1 and sum(mltype) >= 1:
                    tools_index.append(i)
            interop_tools_post = pre_df[(
                'Main', 'tool_name')].iloc[tools_index]
            for j in interop_tools_post.values:
                pre_set.add(j+"_Pre")
            data_stat = [(connect+"_Pre", tool+"_Post")
                            for connect in interop_tools_post.values]
            data = data_stat + data
            seen = set()
            data = [t for t in data if tuple(sorted(t)) not in seen and not seen.add(tuple(sorted(t)))]
        label = []
        for i in range(len(data)):
            label.append(data[i][0])
            label.append(data[i][1])
        label = label + list(annot_set)
        #label_set = set(label)

        uniq_label = []
        for i in label:
            if i not in uniq_label:
                uniq_label.append(i)
        
        target_source_dict = {name: cat for name, cat in list(
            zip(uniq_label, range(0, len(uniq_label))))}
        target = []
        source = []
        value = []
        for i in range(len(data)):
            source.append(target_source_dict[data[i][0]])
            target.append(target_source_dict[data[i][1]])
            value.append(1)
        
        # Create the visual
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=uniq_label,

            ),
            link=dict(
                source=source,
                target=target,
                value=value
            ))])
        if data:
            fig.update_layout(
                title_text="Potential Pipe with" + " " + tool_name, font_size=10)
        else:
            fig.update_layout(
                title_text="No pipeline found for" + " " + tool_name, font_size=10)
        return fig
    else:
        raise PreventUpdate