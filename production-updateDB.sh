#!/bin/sh
text=`cat database_checksum.txt`
textArray=($text)
currentChkSum=`sha1sum sqlDump.sql`
currentChkSumArray=($currentChkSum)
if [[ ${textArray[0]} != ${currentChkSumArray[0]} ]]
then
    echo "Updating database"
    sudo mv pg_sql pg_sql-`date +%s`
    sudo mkdir pg_sql
    sudo chown 999:999 pg_sql
    sudo chmod 700 pg_sql
    echo "$currentChkSum" > database_checksum.txt
else
    echo "No changes made to database"
fi
